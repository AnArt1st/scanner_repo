import 'package:pantryfox/helper/userUtils.dart';
import 'package:pantryfox/model/upcDto.dart';
import 'package:jaguar_orm/jaguar_orm.dart';


/// This is the UPC object to save in the database. We don't want to store everything from the json (See upc.dart).
/// Just enough to find the record and display details.
class UpcDb {
  static final String nullString = "null";

  @PrimaryKey()
  String code; //upc number
  @Column(isNullable: true)
  num total;
  @Column(isNullable: true)
  String title;
  @Column(isNullable: true)
  String description;
  @Column(isNullable: true)
  String imageLink;

  @Column(isNullable: true)
  String cupboard;
  @Column(isNullable: true)
  String brand;
  @Column(isNullable: true)
  String model;
  @Column(isNullable: true)
  String price;
  @Column(isNullable: true)
  String weight;
  @Column(isNullable: true)
  bool selected = false;
  @Column(isNullable: true)
  int entryTime;

  UpcDb({this.code, this.total, this.title, this.description, this.imageLink,
    this.cupboard, this.brand, this.model, this.price, this.weight,
    this.selected, this.entryTime});


  static const String codeName = 'code';
  static const String totalName = 'total';
  static const String titleName = 'title';
  static const String descriptionName = 'description';
  static const String imageLinkName = 'imageLink';

  //new
  static const String cupboardName = 'cupboard';
  static const String brandName = 'brand';
  static const String modelName = 'model';
  static const String priceName = 'price';
  static const String weightName = 'weight';
  static const String selectedName = 'selected';
  static const String entryTimeName = 'entryTime';

  Map<String, dynamic> toJson() => {
    codeName : this.code,
    totalName : this.total,
    titleName : this.title,
    descriptionName : this.description,
    imageLinkName : this.imageLink,
    cupboardName : this.cupboard,
    brandName : this.brand,
    modelName : this.model,
    priceName : this.price,
    weightName : this.weight,
    selectedName : this.selected,
    entryTimeName : this.entryTime,
  };


  factory UpcDb.fromJson(Map<String, dynamic> json) {
    if (json == null) {
      throw FormatException("Null JSON provided to Upc");
    }

    if (json [0] is num) {
      //TODO: Check if ID is correct ID.
      print ('Creating UpcDb from database.');
    }

    // Change this if Json is changed.
    return UpcDb(
        code: json [codeName] == null ? "" : json [codeName],
        total: json [totalName] == null ? 0 : int.parse(json [totalName]),
        title: json[titleName] == null ? "" : json [titleName],
        description: json[descriptionName] == null ? "" : json [descriptionName],
        imageLink: json[imageLinkName] == null ? "" : json [imageLinkName],
        cupboard: json[cupboardName] == null ? "Default Cupboard Name" : json [cupboardName],
        brand: json[brandName] == null ? "" : json [brandName],
        model: json[modelName] == null ? "" : json [modelName],
        price: json[priceName] == null ? "" : json [priceName],
        weight: json[weightName] == null ? "" : json [weightName],
        selected: false,
        entryTime: json[entryTimeName] == null ? 0 : int.parse(json[entryTimeName])
    );
  }

  ///When getting an object from the database using findOne() or findAll(),
  ///those methods return the objects in a Map.
  ///This method extracts the object from the Map and returns an instance of that type.
  static UpcDb getUpcDbFromMap(Map upcMap) {
    if(upcMap == null) return null;
    UpcDb upcDb = new UpcDb();
    upcDb.code = upcMap[UpcDb.codeName];
    upcDb.imageLink = upcMap[UpcDb.imageLinkName];
    upcDb.title = upcMap[UpcDb.titleName];
    if(upcMap[UpcDb.totalName] == null) {
      upcDb.total = 1; // 1 for 'this' one
    } else {
      upcDb.total = int.parse(upcMap[UpcDb.totalName]);
    }
    upcDb.description = upcMap[UpcDb.descriptionName] == null ? "" : upcMap[UpcDb.descriptionName];

    upcDb.cupboard = upcMap[UpcDb.cupboardName];
    upcDb.brand = upcMap[UpcDb.brandName];
    upcDb.model = upcMap[UpcDb.modelName];
    upcDb.price = upcMap[UpcDb.priceName];
    upcDb.weight = upcMap[UpcDb.weightName];
    var selected = (upcMap[UpcDb.selectedName] == null) ? false : upcMap[UpcDb.selectedName];
    upcDb.selected = (selected is bool) ? selected : false;
    if( isNumeric(upcMap[UpcDb.entryTimeName].toString() )) {
      upcDb.entryTime = int.parse(upcMap[UpcDb.entryTimeName].toString());
    } else {
      upcDb.entryTime = 0;
    }
    return upcDb;
  }


  static UpcDb getUpcDb(UpcDto upcDto) {
    if(upcDto == null) {
      return new UpcDb(
          code:         nullString,
          title:        "",
          total:        0,
          description:  "",
          imageLink:    "",
          cupboard:     "",
          brand:        "",
          model:        "",
          price:        "",
          weight:       "",
          selected:     false,
          entryTime:    0
      );
    }
    return new UpcDb(
        code:       upcDto.code,
        title:      upcDto.title,
        total:      upcDto.total,
        description:  upcDto.description,
        imageLink:  upcDto.imageLink,
        cupboard:   upcDto.cupboard,
        brand:      upcDto.brand,
        model:      upcDto.model,
        price:      upcDto.price,
        weight:     upcDto.weight,
        selected:   upcDto.selected,
        entryTime:  upcDto.entryTime
    );
  }

  static List<UpcDb> getUpcDbList(List<UpcDto> upcDtoList) {
    List<UpcDb> upcDbList = new List<UpcDb>();

    for(UpcDto upcDto in upcDtoList) {
      upcDbList.add(getUpcDb(upcDto));
    }
    print("2 getUpcDtoList converted ${upcDbList.length} records");
    return upcDbList;
  }
}