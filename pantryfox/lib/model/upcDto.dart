import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';
import 'package:pantryfox/helper/upcUtils.dart';
import 'package:pantryfox/model/upcDb.dart';

///This is the UI class used to display in view Widgets
class UpcDto extends Equatable {
  ///Same fields as in UpcDb
  final String code; //upc number
  final num total;
  final String title;
  final String description;
  final String imageLink;
  final String cupboard;
  final String brand;
  final String model;
  final String price;
  final String weight;
  final bool selected;
  final int entryTime;

  UpcDto(
      {this.code,
      this.total,
      this.title,
      this.description,
      this.imageLink,
      this.cupboard,
      this.brand,
      this.model,
      this.price,
      this.weight,
      this.selected,
      this.entryTime});

  ///Calls UpcDb.getUpcDbFromMap - see comment there
  static UpcDto getUpcDtoFromMap(Map upcMap) {
    if (upcMap == null) {
      print("upcMap is null in getUpcDto");
      return null;
    }

    UpcDb upcDb = UpcDb.getUpcDbFromMap(upcMap);
    return getUpcDto(upcDb);
  }

  static UpcDto getUpcDto(UpcDb upcDb, [String upcCode = ""]) {
    if (upcDb == null) {
      return new UpcDto(
          code: upcCode.isEmpty ? nullString : upcCode,
          total: 1,
          title: "UPC not found",
          description: "This upc indicate it was not found.",
          imageLink: noImageUrl,
          cupboard: "?",
          brand: "?",
          model: "?",
          price: "0",
          weight: "?",
          selected: false,
          entryTime: Timestamp.now().millisecondsSinceEpoch);
    }
    return new UpcDto(
        code: upcDb.code,
        total: upcDb.total,
        title: upcDb.title,
        description: upcDb.description,
        imageLink: upcDb.imageLink,
        cupboard: upcDb.cupboard,
        brand: upcDb.brand,
        model: upcDb.model,
        price: upcDb.price,
        weight: upcDb.weight,
        selected: upcDb.selected,
        entryTime: upcDb.entryTime);
  }

  static List<UpcDto> getUpcDtoList(List<UpcDb> upcDbList) {
    List<UpcDto> upcDtoList = new List<UpcDto>();

    for (UpcDb upcDb in upcDbList) {
      upcDtoList.add(getUpcDto(upcDb));
    }
    print("1 getUpcDtoList converted ${upcDtoList.length} records");
    return upcDtoList;
  }

  static UpcDto copyWithTotal(UpcDto upcDto, int total) {
    return new UpcDto(
        code: upcDto.code,
        total: total,
        title: upcDto.title,
        description: upcDto.description,
        imageLink: upcDto.imageLink,
        cupboard: upcDto.cupboard,
        brand: upcDto.brand,
        model: upcDto.model,
        price: upcDto.price,
        weight: upcDto.weight,
        selected: upcDto.selected,
        entryTime: upcDto.entryTime);
  }

// ToDo: Add cupboard to details page.
  static UpcDto copyWithUpcDto(UpcDto upcDto, UpcDto newDto) {
    return new UpcDto(
        code: upcDto.code,
        total: newDto.total,
        title: newDto.title,
        description: newDto.description,
        imageLink: upcDto.imageLink,
        cupboard: newDto.cupboard,
        brand: newDto.brand,
        model: newDto.model,
        price: newDto.price,
        weight: newDto.weight,
        selected: upcDto.selected,
        entryTime: upcDto.entryTime);
  }

  @override
  List<Object> get props => [this.code, this.entryTime];
}
