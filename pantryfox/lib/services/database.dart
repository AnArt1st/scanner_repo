import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:pantryfox/model/upcDb.dart';
import 'package:pantryfox/singleton.dart';

class DatabaseService {

  //uid returned from auth
  //ties uid to data
  final String uid;
  DatabaseService({ this.uid });

  // ******** pantryProfileCollection reference *********
  final CollectionReference pantryProfileCollection = Firestore.instance.collection('pantryProfile');

  Future<void> updateUserData(String name, String email, String deviceID, String itemPageSize) async {
      return await pantryProfileCollection.document(uid).collection('user').document(deviceID)
          .setData({
        'name': name,
        'email': email,
        'itemPageSize': itemPageSize,
      });
  }


  // get pantryProfile for this device/user
  Future<Map> getUserProfile(String deviceID) async {
    DocumentReference documentReference = pantryProfileCollection.document(uid).collection('user').document(deviceID);
    DocumentSnapshot userRef = await documentReference.get();
    var data = userRef.data;
    return data;
  }


// ********                         *********
// ******** upcCollection reference *********
// ********                         *********
  final CollectionReference upcCollection = Firestore.instance.collection('upcData');

  Future<void> updateUpcData(UpcDb upcDb) async {
    return await upcCollection.document(uid).collection('upc').document(upcDb.code)
        .setData({
          UpcDb.totalName : upcDb.total,
          UpcDb.titleName : upcDb.title,
          UpcDb.descriptionName: upcDb.description,
          UpcDb.imageLinkName: upcDb.imageLink,
          UpcDb.cupboardName: upcDb.cupboard,
          UpcDb.brandName: upcDb.brand,
          UpcDb.modelName: upcDb.model,
          UpcDb.priceName: upcDb.price,
          UpcDb.weightName: upcDb.weight,
          UpcDb.selectedName: upcDb.selected,
          UpcDb.entryTimeName: upcDb.entryTime,

    });
  }

  ///Take local and insert to firebase
  Future<void> backupUpcData() async {
    List<UpcDb> recordList = await Singleton.instance.upcSqfliteDb.bean.findAll();
    for(UpcDb upcDb in recordList) {
      await updateUpcData(upcDb);
    }
  }

  /// Retrieve firebase upc data and store in Singleton
  /// This method deletes all from sqlLite and inserts to sqlLite records from firebase
  Future<void> restoreUpcData() async {

    QuerySnapshot querySnapshot = await upcCollection.document(uid).collection('upc').getDocuments();
    List<DocumentSnapshot> snapshotList = querySnapshot.documents;
    if(snapshotList == null || snapshotList.isEmpty) {
      return;
    }


    await Singleton.instance.upcSqfliteDb.bean.removeAll();


    //TODO: ???
    //Singleton.instance.upcDbEntries.clear();
    snapshotList.forEach((productDoc) async => await _loadUpc(productDoc));

    await Singleton.instance.upcSqfliteDb.bean.count().then((val) {
      print("Number of records after load from firebase is ${val.toString()}");
    });
    //TODO ??
    //Singleton.instance.firstPage();
  }

  Future<void> _loadUpc(DocumentSnapshot upcDoc) async {
    UpcDb upcDb;
    upcDb = _upcFromDocumentSnapshot(upcDoc);

    await Singleton.instance.upcSqfliteDb.bean.insert(upcDb);
    //don't add all from backup - use paging
    //Singleton.instance.upcDbEntries.add(upcDb);
  }


  UpcDb _upcFromDocumentSnapshot(DocumentSnapshot snapshot) {
    final doc = snapshot.data;
    return new UpcDb(
        brand: doc[UpcDb.brandName],
        code: snapshot.documentID,
        total:doc[UpcDb.totalName],
        title:doc[UpcDb.titleName],
        description:doc[UpcDb.descriptionName],
        imageLink:doc[UpcDb.imageLinkName],
        cupboard:doc[UpcDb.cupboardName],
        model:doc[UpcDb.modelName],
        price:doc[UpcDb.priceName],
        weight:doc[UpcDb.weightName],
        selected:doc[UpcDb.selectedName],
        entryTime:doc[UpcDb.entryTimeName]
    );
  }


  Future<void> createTestUpcDbItem() async {
    UpcDb upcDb = new UpcDb();

    upcDb.total =       99;
    upcDb.title =       UpcDb.titleName;
    upcDb.description = UpcDb.descriptionName;
    upcDb.imageLink =   UpcDb.imageLinkName;
    upcDb.cupboard  =   UpcDb.cupboardName;
    upcDb.brand =       UpcDb.brandName;
    upcDb.model =       UpcDb.modelName;
    upcDb.price =       UpcDb.priceName;
    upcDb.weight =      UpcDb.weightName;
    upcDb.selected =    false;
    upcDb.entryTime =   DateTime.now().millisecondsSinceEpoch;

    await updateUpcData(upcDb);

  }


}




