import 'dart:collection';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:pantryfox/helper/userUtils.dart';
import 'package:pantryfox/model/user.dart';
import 'package:pantryfox/screens/settings/widgets/settings_bloc.dart';
import 'package:pantryfox/services/database.dart';
import 'package:pantryfox/singleton.dart';


class AuthService {
  final FirebaseAuth _auth = FirebaseAuth?.instance;

  // create user obj based on firebase user
  User _userFromFirebaseUser(FirebaseUser user) {
    print("enter _userFromFirebaseUser ${user?.uid ?? "firebase user is null"}");
    return user != null ? User(uid: user.uid) : null;
  }

  // auth change user stream
  Stream<User> get user {
    print("enter get user");
    return _auth.onAuthStateChanged.map(_userFromFirebaseUser);
  }

  Future signInAnon() async {
    print("enter signInAnon");
    try {
      AuthResult authResult = await _auth.signInAnonymously();
      FirebaseUser user = authResult.user;
      Singleton.instance.currentUser = _userFromFirebaseUser(user);
      return Singleton.instance.currentUser;
    } catch(e) {
        print(e.toString());
      return null;
    }
  }

// sign in with email and password
  Future signInWithEmailAndPassword(String email, String password) async {
    print("enter signInWithEmailAndPassword");
    try {
      AuthResult result = await _auth.signInWithEmailAndPassword(email: email, password: password);
      //TODO handle failed sign in
      FirebaseUser user = result.user;
      Singleton.instance.currentUser = _userFromFirebaseUser(user);
      await updateUserWithPantryProfile();
      return Singleton.instance.currentUser;
    } catch (error) {
        print(error.toString());
      return null;
    }
  }

  Future<void> updateUserWithPantryProfile() async {
    print("enter updateUserWithPantryProfile");
    if(Singleton.instance.currentUser == null) {
      print("currentUser is null Login - did login fail??");
      return;
    }
    String device = await deviceName();
    LinkedHashMap pantryProfile;
    pantryProfile = await Singleton.instance.firebaseService.getUserProfile(device);

    String name = Singleton.instance.currentUser?.name;
    String pageSize = Singleton.instance.currentUser?.itemPageSize?.toString();
    String email = Singleton.instance.currentUser?.email ?? "unknown email";
    if(pantryProfile == null) {
      //no pantry profile, add one
      await Singleton.instance.firebaseService.updateUserData(
          name ?? "Your Name Here", email, device, pageSize ?? SettingsFormBloc.defaultPageSize);
      //get profile just added
      pantryProfile = await Singleton.instance.firebaseService.getUserProfile(device);
    }

    if(pantryProfile == null) {
      print("Error! pantryProfile is null");
      return;
    }

    //update currentUser with pantryProfile to make sure they're in sync
    name = pantryProfile['name'];
    Singleton.instance.currentUser.name = name ?? "Your Name Here";

    pageSize = pantryProfile['itemPageSize'];
    Singleton.instance.currentUser.itemPageSize = (pageSize == null ? int.parse(SettingsFormBloc.defaultPageSize) : int.parse(pageSize));

  }


// register with email and password
  Future<User> registerWithEmailAndPassword(String email, String password, GlobalKey<ScaffoldState> scaffoldKey) async {
    print("enter registerWithEmailAndPassword");
    User upcUser;
    try {
      AuthResult result = await _auth.createUserWithEmailAndPassword(email: email, password: password);
      FirebaseUser user = result.user;
      upcUser = _userFromFirebaseUser(user);
      Singleton.instance.currentUser = upcUser;
      await updateUserWithPantryProfile();
      ///TODO: test only
      //await createTestUpcData(upcUser);
    } catch (error) {
      debugPrint(error.toString());
      showToast(scaffoldKey, error.toString());
    }
    return upcUser;
  }


// sign out
  Future signOut() async {
    print("enter signOut");
    try {
      return await _auth.signOut();
    } catch (error) {
      print(error.toString());
      return null;
    }
  }

  /// Just for testing
  createTestUpcData(User user) {
    print("Inserting test UpcDb ********** ");
    DatabaseService service = new DatabaseService(uid: user.uid);
    service.createTestUpcDbItem();
  }

}