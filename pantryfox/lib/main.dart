import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pantryfox/helper/userUtils.dart';
import 'package:pantryfox/model/upcDto.dart';
import 'package:pantryfox/screens/authwrapper.dart';
import 'package:pantryfox/screens/create_user/create_user.dart';
import 'package:pantryfox/screens/homepage/homePage.dart';
import 'package:pantryfox/screens/login/login.dart';
import 'package:pantryfox/screens/settings/settings.dart';
import 'package:pantryfox/screens/settings/widgets/settings_bloc.dart';
import 'package:pantryfox/screens/upc_details/details.dart';
import 'screens/login/widgets/login_bloc.dart';
import 'bloc/theme_bloc.dart';
import 'bloc/upc_event_state_bloc.dart';
import 'repository/upc_repository.dart';

import 'screens/authwrapper.dart';

var localizationData;

class SimpleBlocObserver extends BlocObserver {
  @override
  void onEvent(Bloc bloc, Object event) {
    print(event);
    super.onEvent(bloc, event);
  }

  @override
  void onTransition(Bloc bloc, Transition transition) {
    print(transition);
    super.onTransition(bloc, transition);
  }

  @override
  void onError(Bloc bloc, Object error, StackTrace stackTrace) {
    print(error);
    super.onError(bloc, error, stackTrace);
  }
}

void main() {
  // BlocSupervisor.delegate = SimpleBlocDelegate();
  Bloc.observer = SimpleBlocObserver();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
        providers: [
          BlocProvider<ThemeBloc>(
              create: (BuildContext context) => ThemeBloc(getTheme())),
          BlocProvider<LoginBloc>(
              create: (BuildContext context) =>
                  BlocProvider.of<LoginBloc>(context)),
          BlocProvider<SettingsFormBloc>(
              create: (BuildContext context) =>
                  BlocProvider.of<SettingsFormBloc>(context)),
          BlocProvider<UpcEventStateBloc>(
              create: (BuildContext context) =>
                  UpcEventStateBloc(UpcRepository())),
        ],
        // ChildA()
        child: BlocBuilder<ThemeBloc, ThemeData>(
          builder: (context, theme) {
            return MaterialApp(
              title: 'PantryFox',
              home: AuthWrapper(),
              theme: getTheme(),
              routes: {
                '/scanHomePage': (context) => ScanHomePage(),
                '/loginPage': (context) => LoginPage(),
                '/settings': (context) => SettingsPage(),
                '/createUser': (context) => CreateUser(),
                // '/upcDetails': (context) => DetailsPage(UpcDto())
              },
              debugShowCheckedModeBanner: false,
              // themeMode: ThemeMode.system, //maybe?
            );
          },
        ));
  }
}

ThemeData getTheme() {
  return ThemeData(
    primaryColor: myColorArray[1] ?? Colors.blueAccent[100],
    accentColor: myColorArray[0] ?? Colors.yellow[200],
    backgroundColor: myColorArray[0],
    textTheme: TextTheme(
      bodyText2: TextStyle(
        color: Colors.black,
        fontFamily: 'Times',
      ),
      button: TextStyle(
        color: Colors.white,
        fontFamily: 'Times',
      ),
      headline6: TextStyle(
        color: Colors.white,
        fontFamily: 'Times',
      ),
    ),
  );
}
