import 'dart:async';

import 'package:pantryfox/controller/upc_database.dart';
import 'package:pantryfox/helper/userUtils.dart';
import 'package:pantryfox/model/upcDb.dart';
import 'package:pantryfox/model/upcDto.dart';

import '../singleton.dart';

abstract class UpcRepositoryInterface {
  Future<List<UpcDto>> fetchPageList({int offSet, int fetchQty, int totalQty});
  //List<UpcDto> allRows();
  //UpcDto fetchUpc(String upcCode);
  Future<UpcDto> findUpcDtoInDatabase(String code);
  Future<int> count(); //count of rows in database
  Future<void> updateTotalByOne(UpcDto upcDto, int value);
  Future<void> updateUpcDto(UpcDto upcDto);
  Future<void> setTotal(UpcDto upcDto, int value);
  Future<void> removeOne(UpcDto upcDto);
  Future<void> addUpcToDevice(UpcDto upcDto, String goodImageUrl);
}

class UpcRepository implements UpcRepositoryInterface {
  UpcSqflite upcSqflite = Singleton.instance.upcSqfliteDb;

  get f => null;

//  @override
//  List<UpcDto> allRows() async {
//    List<UpcDb> records = await upcSqflite.bean.findAll();
//    print("Start allRows. #records returned = ${records.length}");
//
//    return UpcDto.getUpcDtoList(records);
//  }

  @override
  Future<List<UpcDto>> fetchPageList(
      {int offSet, int fetchQty, int totalQty}) async {
    debugPrinter(
        "Start fetchPageList. totalQty = $totalQty offsetPos = $offSet, fetchQty = $fetchQty ");
    if (totalQty == null || totalQty == 0) {
      return Future<List<UpcDto>>.value(new List<UpcDto>());
    }
    List<UpcDb> upcDtoList =
        await upcSqflite.bean.nextPage(offset: offSet, fetchQty: fetchQty);
    debugPrinter(
        "_nextPage() outside then #records returned = ${upcDtoList == null ? 0 : upcDtoList.length}");
    return upcDtoList == null
        ? Future<List<UpcDto>>.value(new List<UpcDto>())
        : UpcDto.getUpcDtoList(upcDtoList);
  }

  @override
  Future<int> count() async {
    int count = await upcSqflite.bean.count();
    return count;
  }

  Future<UpcDto> findUpcDtoInDatabase(String code) async {
    UpcDb upcDb = await upcSqflite.bean.findOne(code);
    return UpcDto.getUpcDto(upcDb);
  }

  Future<void> updateTotalByOne(UpcDto upcDto, int value) async {
    UpcDb upcDb = await upcSqflite.bean.findOne(upcDto.code);
    if (upcDb == null) {
      debugPrinter("updateTotalByOne ERROR - cannot find upcDb in datahase!");
      return;
    }
    await upcSqflite.bean.updateTotal(upcDb, value);
  }

  /// Is this the same as updateTotalByOne?
  @override
  Future<void> setTotal(UpcDto upcDto, int value) async {
    UpcDb upcDb = await upcSqflite.bean.findOne(upcDto.code);
    if (upcDb == null) {
      debugPrinter("updateTotalByOne ERROR - cannot find upcDb in datahase!");
      return;
    }
    await upcSqflite.bean.setTotal(upcDb, value);
  }

  Future<void> removeOne(UpcDto upcDto) async {
    UpcDb upcDb = await upcSqflite.bean.findOne(upcDto.code);
    if (upcDb == null) {
      debugPrinter("removeOne ERROR - cannot find upcDb in datahase!");
      return;
    }
    await upcSqflite.bean.removeOne(upcDb.code);
  }

  Future<void> addUpcToDevice(UpcDto upcDto, String goodImageUrl) async {
    debugPrinter("start addUpcToDevice");
    await upcSqflite.bean.addUpcToDevice(UpcDb.getUpcDb(upcDto), goodImageUrl);
  }

  /// upcDto is an updated UpcDto.
  @override
  Future<void> updateUpcDto(UpcDto upcDto) async {
    if (upcDto == null) {
      debugPrinter("Failed to update UpcDto in repository.");
      return;
    }
    UpcDb upcDb = UpcDb.getUpcDb(upcDto);
    await upcSqflite.bean.updateUpcDbFields(upcDb, true);
  }
}
