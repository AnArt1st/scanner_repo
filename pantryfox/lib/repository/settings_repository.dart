import 'package:pantryfox/model/settingsDto.dart';
import '../singleton.dart';

abstract class SettingsRepositoryInterface{
  Future<void> setPageSize(SettingsDto settingsDto, int pageSize);
  Future<void> setOrderBy(SettingsDto settingsDto, String goodImageUrl);
}


class SettingsRepository implements SettingsRepositoryInterface {

//  UpcSqflite upcSqflite = Singleton.instance.upcSqfliteDb;




  Future<void> setPageSize(SettingsDto settingsDto, int pageSize) async {

  }


  Future<void> setOrderBy(SettingsDto settingsDto, String orderBy) async {

  }


}