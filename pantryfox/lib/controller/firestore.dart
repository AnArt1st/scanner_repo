//import 'package:firebase_database/firebase_database.dart';
//import 'package:cloud_firestore/cloud_firestore.dart';
//import 'package:pantryfox/model/upc.dart';
//
//import '../singleton.dart';
//
//Future<void> init(FirebaseDatabase database) async {
//
//  //counterRef = FirebaseDatabase.instance.reference().child('test/counter');
//  //counterRef.keepSynced(true);
//  //database in Singleton
//  print("************  Firebase init called in app ***********");
//  print("************  Firebase init called in app ***********");
//  Upc upc = new Upc();
//  Singleton.instance.upc = upc;
//  database.setPersistenceEnabled(true);
//  database.setPersistenceCacheSizeBytes(5000000);
//}
//
//
//Future<Upc> getUpc() async {
//  DatabaseReference _messageRef;
//  _messageRef = FirebaseDatabase.instance.reference().child('messages/${Singleton.instance.firebaseuser}');
//  Upc value;
//  value = await getTeaData(_messageRef);
//
//  return value;
//}
//
//
////Future<ItemModel> getTeaBlocModel() async {
////  DatabaseReference _messageRef;
////  ItemModel value;
////
////  if(Singleton.instance.tea.products.length > 0) {
////    print("************  creating tea model from singleton data ***********");
////    value = new ItemModel.fromProductList(Singleton.instance.tea.products);
////    return value;
////  }
////  _messageRef = FirebaseDatabase.instance.reference().child('messages/${Singleton.instance.firebaseuser}');
////
////  value = await getTeaBlocData(_messageRef);
////
////  return value;
////}
////
////
////
////Future<ItemModel> getTeaBlocData(DatabaseReference _databaseRef) async {
////
////  if(Singleton.instance.loggedIn == false) {
////    print("getTeaBlocData - WARNING not logged in");
////    return new ItemModel(null);
////  }
////
//////  var categoriesDoc  = await Firestore.instance.collection("tea_data").document('categories').get();
//////  tea.categories = _categoriesFromDocumentSnapshot(categoriesDoc);
////
////  QuerySnapshot querySnapshot = await Firestore.instance.collection("tea_data").document('products').collection("tea_products").getDocuments();
////
////  print("************  creating tea model from querySnapshot  ***********");
////  ItemModel tea = new ItemModel(querySnapshot);
////
//////  var recipesDoc  = await Firestore.instance.collection("tea_data").document('recipes').get();
//////  tea.recipes = _recipesFromDocumentSnapshot(recipesDoc);
////
////
////  return tea;
////}
//
//
//Future<Upc> getTeaData(DatabaseReference _databaseRef) async {
//
//  Upc upc = new Upc();
//  Singleton.instance.upc = upc;
//
//  if(Singleton.instance.loggedIn == false) {
//    return upc;
//  }
//
//  var categoriesDoc  = await Firestore.instance.collection("tea_data").document('categories').get();
//  //upc.categories = _categoriesFromDocumentSnapshot(categoriesDoc);
//
//  QuerySnapshot querySnapshot = await Firestore.instance.collection("tea_data").document('products').collection("tea_products").getDocuments();
//  List<DocumentSnapshot> list = querySnapshot.documents;
//  if(list != null && list.length>0) {
//    list.forEach((productDoc) => _loadProduct(productDoc));
//  }
//
//  var recipesDoc  = await Firestore.instance.collection("tea_data").document('recipes').get();
//  //upc.recipes = _recipesFromDocumentSnapshot(recipesDoc);
//
//
//  return upc;
//}
//
//void _loadProduct(DocumentSnapshot productDoc) {
//
//  //Product product;
//  //product = _productFromDocumentSnapshot(productDoc);
//  //Singleton.instance.tea.products.add(product);
//  //persnickStorage.loadTeaImage(product.tLeavesImage);
//}
//
////Categories _categoriesFromDocumentSnapshot(DocumentSnapshot snapshot) {
////  final data = snapshot.data;
////
////  return new Categories(
////    documentId: snapshot.documentID,
////    black: data['black'],
////    chai: data['chai'],
////    green: data['green'],
////    oolong: data['oolong'],
////    puErh: data['puErh'],
////    rooibos: data['rooibos'],
////    white: data['white'],
////  );
////}
////
////Product _productFromDocumentSnapshot(DocumentSnapshot snapshot) {
////  final data = snapshot.data;
////
////  return new Product(
////    documentId: snapshot.documentID,
////    categories: new List<String>.from(data['categories']),
////    duration: data['duration'],
////    ingredients: new List<String>.from(data['ingredients']),
////    pDescript: data ['pDescript'],
////    pUrl: data ['pUrl'],
////    pname: data ['pname'],
////    prepMethod: new List<String>.from(data['prepMethod']),
////    qty: data ['qty'],
////    sDescript: data ['sDescript'],
////    tCupView: data ['tCupView'],
////    tLeavesImage: data ['tLeavesImage'],
////    teaType: data ['teaType'],
////    temperature: data ['temperature'],
////
////  );
////}
////
////Recipes _recipesFromDocumentSnapshot(DocumentSnapshot snapshot) {
////  final data = snapshot.data;
////
////  return new Recipes(
////    documentId: snapshot.documentID,
////    iced: new List<String>.from(data['iced']),
////    infuser: new List<String>.from(data['infuser']),
////  );
////}