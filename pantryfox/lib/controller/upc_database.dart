
import 'package:pantryfox/screens/settings/widgets/settings_bloc.dart';
import 'package:pantryfox/helper/upcUtils.dart';
import 'package:pantryfox/helper/userUtils.dart';
import 'package:pantryfox/model/upcDb.dart';
import 'package:jaguar_orm/jaguar_orm.dart';
import 'package:jaguar_query_sqflite/jaguar_query_sqflite.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:async';
import '../singleton.dart';

//examples: https://github.com/jaguar-orm/sqflite

/// The adapter
SqfliteAdapter _upcAdapter;

///Name of the file/database holding scanned data - unit tests can change it as needed
///when adding or removing columns from the schema, need to increment filename by 1
String upcFileName = "/upcFile8.json";

class UpcSqflite {

  UpcBean bean;

  init(String databaseFileName) async {
    if(_upcAdapter == null) {
      if(databaseFileName != null) {
        upcFileName = databaseFileName;
      }
      _upcAdapter = new SqfliteAdapter(await getDatabasesPath()+upcFileName, version: 1);
      //_adapter.
    }
    if(bean == null) {
      try {
        await _upcAdapter.connect();
      } catch(e) {
        debugPrinter("Cannot connect to database");
        debugPrinter(e.toString());
      }
      bean = new UpcBean();
    }
    await bean.createTable();
  }

  close() async {
    await _upcAdapter.close();
  }

  // Count items scanned.
  Future<int> countItemsScanned() async {
    if(bean == null) {
      await init(null);
    }
    int count = await bean.count();
    debugPrinter('There are $count scanned into DB.');
    return count;
  }
}


class UpcBean {
  /// DSL Fields
  final StrField code = new StrField(UpcDb.codeName);
  final IntField total = new IntField(UpcDb.totalName);
  final StrField title = new StrField(UpcDb.titleName);
  final StrField description = new StrField(UpcDb.descriptionName);
  final StrField imageLink = new StrField(UpcDb.imageLinkName);
  //new
  final StrField cupboard = new StrField(UpcDb.cupboardName);
  final StrField brand = new StrField(UpcDb.brandName);
  final StrField model = new StrField(UpcDb.modelName);
  final StrField price = new StrField(UpcDb.priceName);
  final StrField weight = new StrField(UpcDb.weightName);
  final BoolField selected = new BoolField(UpcDb.selectedName);
  final IntField entryTime = new IntField(UpcDb.entryTimeName);

  setTimeToIntField(int millisecondsSinceEpoch) {

  }

  /// Table name for the model this bean manages
  String get upcTableName => 'upc_table';

  /// Inserts a new record into table
  Future insert(UpcDb item) async {

    final insert = Insert(upcTableName).setValues({
      UpcDb.codeName : item.code,
      UpcDb.totalName : item.total,
      UpcDb.titleName : item.title,
      UpcDb.descriptionName: item.description,
      UpcDb.imageLinkName: item.imageLink,
      UpcDb.cupboardName: item.cupboard,
      UpcDb.brandName: item.brand,
      UpcDb.modelName: item.model,
      UpcDb.priceName: item.price,
      UpcDb.weightName: item.weight,
      UpcDb.selectedName: item.selected,
      UpcDb.entryTimeName: item.entryTime,
    });

    return await insert.exec(_upcAdapter); //_adapter.insert(inserter);
  }

  Future<Null> createTable() async {
    final upcTableEntry = new Create(upcTableName, ifNotExists: true);
        upcTableEntry.addStr(UpcDb.titleName, isNullable: true)
            .addStr(UpcDb.totalName, isNullable: true)
            .addStr(UpcDb.descriptionName, isNullable: true)
            .addStr(UpcDb.imageLinkName, isNullable: true)
            .addStr(UpcDb.cupboardName, isNullable: true)
            .addStr(UpcDb.brandName, isNullable: true)
            .addStr(UpcDb.modelName, isNullable: true)
            .addStr(UpcDb.priceName, isNullable: true)
            .addStr(UpcDb.weightName, isNullable: true)
            .addBool(UpcDb.selectedName, isNullable: true)
            .addInt(UpcDb.entryTimeName, isNullable: true)
            .addPrimaryStr(UpcDb.codeName);

    await _upcAdapter.createTable(upcTableEntry);
  }

  /// Finds one post by [code]
  Future<UpcDb> findOne(String code) async {
    Find updater = new Find(upcTableName);
    updater.where(this.code.eq(code));
    Map upcMap = await _upcAdapter.findOne(updater);
    if(upcMap == null || upcMap.isEmpty) return null;

    UpcDb upc = UpcDb.getUpcDbFromMap(upcMap);
    return upc;
  }

  /// Group of rows determined by offset
  Future<List<UpcDb>> nextPage({int offset, int fetchQty}) async {
    //https://github.com/Jaguar-dart/jaguar_orm/wiki/Find-statement
    String orderPref = Singleton.instance.prefs?.getString(SettingsFormBloc.orderBy.toLowerCase()) ?? "title";
    Find finder = new Find(upcTableName).orderBy(orderPref).offset(offset).limit(fetchQty);

    List<Map> maps = (await _upcAdapter.find(finder)).toList();
    List<UpcDb> upcDbsList = new List<UpcDb>();
    UpcDb upcDb;
    for (Map map in maps) {
      upcDb = UpcDb.getUpcDbFromMap(map);
      if(upcDb == null) continue;
      upcDbsList.add(UpcDb.getUpcDbFromMap(map));
    }
    return upcDbsList;
  }

  Future<List<UpcDb>> findAll() async {
    Find finder = new Find(upcTableName);

    List<Map> maps = (await _upcAdapter.find(finder)).toList();
    List<UpcDb> upcDbsList = new List<UpcDb>();
    UpcDb upcDb;
    for (Map map in maps) {
      upcDb = UpcDb.getUpcDbFromMap(map);
      if(upcDb == null) continue;
      upcDbsList.add(upcDb);
    }

    return upcDbsList;
  }

  Future<int> count()  async {
    Find finder = new Find(upcTableName).count(UpcDb.codeName);
    int value=0;
    try {
      Map map = (await _upcAdapter.find(finder)).first;
      value = map['COUNT(code)'];
    } catch (e) {
      debugPrinter(e);
    }

    return value == null ? 0 : value;
  }

  Future<void> setTotal (UpcDb upcDb, int totalAmt) async {
    UpcDb foundInList = await findOne(upcDb.code);
    if(foundInList != null) {
      foundInList.total = totalAmt;
      /// Set selected index
      foundInList.selected = true;

      Update updater = new Update(upcTableName);
      updater.where(this.code.eq(foundInList.code));

      updater.set(this.total, foundInList.total);
      updater.set(this.selected, foundInList.selected);
      await _upcAdapter.update(updater);
      upcDb.total = foundInList.total;
      return foundInList.total;
    } else {
      debugPrinter("setTotal: Did not find upcDb in list");
    }
  }

  /// Only updates the total
  Future<int> updateTotal(UpcDb upcDb, int totalAmt) async {

    UpcDb foundInList = await findOne(upcDb.code);
    if(foundInList != null) {
      foundInList.total += totalAmt;
      //Singleton.instance.upcUniqueQty += totalAmt;
      debugPrinter("after updateTotal, upcQty = ${foundInList.total}.");
      /// Set selected index
      foundInList.selected = true;
      /// Should timeStamp be reset too?
      /// Maybe we store an array of DateTimes?

      Update updater = new Update(upcTableName);
      updater.where(this.code.eq(foundInList.code));

      updater.set(this.total, foundInList.total);
      updater.set(this.selected, foundInList.selected);
      await _upcAdapter.update(updater);
      upcDb.total = foundInList.total;
      return foundInList.total;
    } else {
      debugPrinter("updateTotal: Did not find upcDb in list");
    }
    return 0;
  }

  Future<void> updateUpcDbFields(UpcDb upcDb, bool foundInList) async {
    if (upcDb == null) {
      debugPrinter('Null upcDb passed in. UpcDb was not updated.');
      return;
    }
    if(!foundInList) {
      debugPrinter('Upc not found: UpcDb was not updated.');
      return;
    }

    Update updater = new Update(upcTableName);
    if (updater.where(this.code.eq(upcDb.code)) != null) {
      updater.where(this.code.eq(upcDb.code));

      updater.set(this.total, upcDb.total);
      updater.set(this.code, upcDb.code);
      updater.set(this.title, upcDb.title);
      updater.set(this.description, upcDb.description);
      updater.set(this.imageLink, upcDb.imageLink);
      updater.set(this.brand, upcDb.brand);
      updater.set(this.model, upcDb.model);
      updater.set(this.weight, upcDb.weight);
      updater.set(this.selected, upcDb.selected);
      updater.set(this.entryTime, upcDb.entryTime);
      await _upcAdapter.update(updater);
    } else {
      debugPrinter('Upc matched null: UpcDb was not updated.');
    }
  }

  // Removes one by upc code.
  Future<void> removeOne(String barCode) async {
    Remove deleter = new Remove(upcTableName);
    deleter.where(this.code.eq(barCode));
    return await _upcAdapter.remove(deleter);
  }

  Future<int> removeAll() async {
    Remove deleter = new Remove(upcTableName);
    return await _upcAdapter.remove(deleter);
  }


  ///return false if found in database - that is, not added to database because found
  ///may want to add exception handler if db fails
  Future<UpcDb> addUpcToDevice(UpcDb upcDb, String goodImageUrl) async {

    if (upcDb.code == null || upcDb.code.isEmpty || upcDb.code == nullString) {
      debugPrinter ('Failed to add upcDb to device.');
      return upcDb;
    }

    UpcDb foundUpc = await findOne(upcDb.code);
    if(foundUpc != null && foundUpc.code == upcDb.code) {
      int total = foundUpc.total;
      debugPrinter("found ${foundUpc.code} in database! count = $total");

      await updateTotal(foundUpc, 1);
      if (foundUpc.total == total) {
        debugPrinter('After update, the value of total was still the same. Set the state or something.');
      }
      return foundUpc;
    }

    upcDb.imageLink = goodImageUrl==null ? '' : goodImageUrl;
    await insert(upcDb);
    debugPrinter ('Added upcDb to device.');
    //added upc
    return upcDb;
  }

}





