import 'dart:convert';
import 'dart:io';
import 'package:pantryfox/helper/userUtils.dart';
import 'package:pantryfox/model/upcJson.dart';
import 'package:pantryfox/model/upcDb.dart';
import 'package:pantryfox/services/database.dart';
import 'package:pantryfox/singleton.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:core';

class UpcHttpController {

  final GOOD_RESPONSE=200;
  final REDIRECTION=300;
  final CLIENT_ERROR=400;
  final SERVER_ERROR=500;

//  var apiKeyToken = "/8512882268C5BD916F5B9BB664367EB3";

  var url = "https://api.upcitemdb.com/prod/trial/lookup?upc=";

  //from site https://www.upcitemdb.com/api/explorer#!/lookup/get_trial_lookup
  //NEW https://api.upcitemdb.com/prod/trial/lookup?upc=0885909950805


  //corn 0041498115005
  Future<Upc> getFutureUpcData(String upcId) async {

//    if (upcId.length > 12) {
//      upcId = upcId.substring(upcId.length - 12);
//    }

    // Building work for all barcodes?
    String fullUrl = url+upcId;
    debugPrinter(fullUrl);
    http.Response response;
    try {
      final client = new HttpClient();
      client.connectionTimeout = const Duration(seconds: 10);
      client.getUrl(Uri.parse(fullUrl));
      try {
        response = await http.get(fullUrl);
      } catch (e) {
        print(e);
      }
      // Check response status code.
      if ( response.statusCode >= GOOD_RESPONSE && response.statusCode < REDIRECTION) {
        // Good response.
        // handle data
        Upc goodResponseUpc = getUpcOnGoodResponse(response.body);
        if (goodResponseUpc.items.isEmpty) {
          debugPrinter('Not good. Try EAN or something.');
        }
        return goodResponseUpc;
      } else {
        // Something bad happened.
        if (response.statusCode >= SERVER_ERROR ) {
          //popup error and say try again later - probably toast
          debugPrinter('No Upc because of Server Error');
        } else if (response.statusCode >= CLIENT_ERROR) {
          debugPrinter('UPC not found: Client Error');
        } else {
          debugPrinter('UPC not found: Lost in Redirection');
        }
        debugPrinter('Error creating upc in getFutureUpcData. Returning null.');
        /// Check for this
//      return new Upc();
//        getFutureUpcData(upcId)
        return null;
      }
    } on TimeoutException catch (_) {
      // A timeout occurred.
      debugPrinter('Timeout occured');
      return null;
    } on SocketException catch (_) {
      // Other exception
      debugPrinter('Socket Exception occured');
      return null;
    }
  }

  Upc getUpcOnGoodResponse(String body) {
    // If the call to the server was successful, parse the JSON
    var upcJson = json.decode(body); // Getting stuck here.
    Upc upcthing = Upc.fromJson(upcJson);
    if (upcthing == null) {
      debugPrinter ('Maybe it was NOT a good response!');
    }

    return upcthing;
  }

  /// Await a response.
  Future<void> updatePersnicketyServer () async {
    DatabaseService service = Singleton.instance.firebaseService;
    service.backupUpcData();
  }

  Future<void> fetchPersnicketyData () async {
    DatabaseService service = Singleton.instance.firebaseService;
    await service.restoreUpcData();
  }

  // Get data from db
//  Future<Map<String, dynamic>> fetchPersnicketyData (BuildContext context, String userID) async {
//    //List<Map<String, dynamic>> jsonList = new List<Map<String, dynamic>>();
//
//    String fullUrl = 'https://persnicketysoft.com/getBackup.php';
//    userID = "1c9a8c288f017754173737";
//    var body = ({
//      "userID": userID,
//    });
//
//    http.Response response = await http.post(
//      Uri.encodeFull(fullUrl),
//      body: body,
//      headers: {'Content-type': 'application/x-www-form-urlencoded'},
////      encoding: fullUrl
//    );
//
//    // Check response status code.
//    if ( response.statusCode >= GOOD_RESPONSE && response.statusCode < REDIRECTION) {
//      // Good response.
//      // handle data
//      print('Got a good response from fetch.');
//
//      //TODO
//      //convert json from server to UPC objects and insert into sqflite database
//      final parsed = json.decode(response.body);
//
////      UpcDb upcDb = UpcDb.fromJson(parsed["testData"][0]);
////      print(upcDb.title);
//
//      return parsed;
//      //final data = parsed["testData"];
//      //return data;
//
//    } else {
//      // Something bad happened.
//      if (response.statusCode >= SERVER_ERROR ) {
//        //popup error and say try again later - probably toast
//        print('Server Error');
//      } else if (response.statusCode >= CLIENT_ERROR) {
//        print('Client Error');
//      } else {
//        print('Lost in Redirection');
//      }
//    }
//    print('An error or exception occured.');
//    return null;
//  }



  ///If returns null then use noImage
  Future<String> getWorkingImageUrl(Upc upc) async {
    if(upc == null || upc.items.isEmpty) {
      debugPrinter('No items, so no images.');
      return null;
    }
    if (upc.items[0].images == null || upc.items[0].images.isEmpty) {
      debugPrinter('No images 1');
      return null;
    }

    List<String> images = upc.items[0].images;
    for(int i = 0; i<images.length; i++) {
      var url = await checkImage(images[i]);
      if(url != null) {
        return images[i];
      }
      if (i > 1) break;
    }
    debugPrinter('No images 2');
    return null;
  }



  Future<String> checkImage(String imgUrl) async {
    
    try {
      final response = await http.get(imgUrl);
      if ( response.statusCode >= GOOD_RESPONSE && response.statusCode < CLIENT_ERROR) {
      return imgUrl;
    }
    } on Exception {
      debugPrinter("checkImage: Image not found $imgUrl");
      return null;
    }
    debugPrinter("checkImage: Image not found $imgUrl");
    return null;
  }

  ///Given a Upc from json, return the DB version
  getUpcDb(Upc upc) {
    if(upc == null) return null;

    UpcDb upcDb = new UpcDb(code: upc.code);
    if( upc.items!= null && upc.items.isNotEmpty) {
      Item item = upc.items[0];
      upcDb.code = item.upc;
      upcDb.total = 1; //represents itself, it will be added to total later
      upcDb.title = item.title ?? '';
      upcDb.description = item.description ?? '';
      upcDb.brand = item.brand ?? '';
      upcDb.model = item.model ?? '';
      upcDb.price = getPrice(upc);
      // print?
      upcDb.weight = item.weight ?? '';
      if(item.images.isNotEmpty) {
        upcDb.imageLink = item.images[0];
      } else {
        upcDb.imageLink = 'https://uae.microless.com/cdn/no_image.jpg';
      }
    }
    if (upcDb.code == null || upcDb.title == null) {
      debugPrinter('Check getUpcDb for error.');
    }
    upcDb.entryTime = DateTime.now().millisecondsSinceEpoch;
    return upcDb;
  }

  void debugDumpScan(Upc upc) {
    int itemIndex = 0;
    if (upc == null || upc.items.isEmpty || upc.items is String) {
      debugPrinter('No items found in Upc 1.');
      return;
    } else {
      debugPrinter('debugDumpScan was called with something.');
    }
    if (upc.items[itemIndex] == null || upc.items[itemIndex] is String) {
      debugPrinter('No items found in Upc 2.');
      return;
    }
    double offerPrice = upc.items[0].offers[0] ?? 0.420;

    double listPrice = upc.items[0].offers[0].listPrice ?? 0.420;
    for(var item in upc.items) {
      debugPrinter('/////////');
      debugPrinter('Item Index: ' + itemIndex.toString());
      debugPrinter('EAN: '+ item.ean);
      debugPrinter('ASIN: '+ item.asin);
      debugPrinter('GTIN: '+ item.gtin);
      debugPrinter("Item: ${item.title}");
      debugPrinter("Upc: ${item.upc}");
      debugPrinter("Description: ${item.description}");
      debugPrinter('Price High: \$' + item.highestRecordedPrice.toDouble().toString());
      debugPrinter('Price Low: \$' + item.lowestRecordedPrice.toDouble().toString());
      debugPrinter('Offer Price: \$' + offerPrice.toString());
      debugPrinter('List Price (*May be buggy): \$' + listPrice.toString());
      debugPrinter('/////////');
      itemIndex++;
    }
  }


  String getPrice(Upc upc) {
    if (upc.items.isEmpty || upc.items == null) {
      return "0.0";
    }
    if (upc.items[0].offers == null || upc.items[0].offers.isEmpty) {
      debugPrinter('getPrice. Error?');
      if (upc.items[0].lowestRecordedPrice==null || upc.items[0].highestRecordedPrice==null) {
        return "0.0";
      }
        if (upc.items[0].lowestRecordedPrice > 0) {
          return upc.items[0].lowestRecordedPrice.toString();
        } else if (upc.items[0].highestRecordedPrice > 0) {
          return upc.items[0].highestRecordedPrice.toString();
        } else {
          return "0.0";
        }
    }
      if (upc.items[0].offers[0].price == null || upc.items[0].offers[0].price == 0.0) {
        if (upc.items[0].offers[0].listPrice == null) {
          return "0.0";
        } else {
          return upc.items[0].offers[0].listPrice.toString();
        }
      } else {
        return upc.items[0].offers[0].price.toString();
      }
  } // Tries to return a price greater than 0.0

} //End class


class SetUpJson {
//  String optionID;
  Map<String, dynamic> data;
//  String data;
  SetUpJson(this.data);
  Map<String, dynamic> toJsonData() {
    var map = new Map<String, dynamic>();
//    map["optionID"] = optionID;
    map["data"] = data;

    return map;
  }
}