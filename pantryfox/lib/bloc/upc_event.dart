import 'package:equatable/equatable.dart';
import 'package:pantryfox/model/upcDto.dart';

abstract class UpcEvent extends Equatable {
  const UpcEvent();
}

class FirstPageEvent extends UpcEvent implements ItemPagination, UpcPageEvent {
  final int offSet;
  final int fetchQty;

  const FirstPageEvent({this.offSet, this.fetchQty});

  @override
  List<Object> get props => [offSet, fetchQty];
}

class PrevPageEvent extends UpcEvent implements ItemPagination, UpcPageEvent {
  final int offSet;
  final int fetchQty;

  const PrevPageEvent({this.offSet, this.fetchQty});

  @override
  List<Object> get props => [offSet, fetchQty];
}

class NextPageEvent extends UpcEvent implements ItemPagination, UpcPageEvent {
  final int offSet;
  final int fetchQty;

  const NextPageEvent({this.offSet, this.fetchQty});

  @override
  List<Object> get props => [offSet, fetchQty];
}

// LastPageEvent
class LastPageEvent extends UpcEvent implements ItemPagination, UpcPageEvent {
  final int offSet;
  final int fetchQty;

  const LastPageEvent({this.offSet, this.fetchQty});

  @override
  List<Object> get props => [offSet, fetchQty];
}

class IncrementItemEvent extends UpcEvent implements UpcPageEvent {
  final UpcDto upcDto;

  const IncrementItemEvent({this.upcDto});

  @override
  List<Object> get props => [upcDto];
}

class DecrementItemEvent extends UpcEvent implements UpcPageEvent {
  final UpcDto upcDto;

  const DecrementItemEvent({this.upcDto});

  @override
  List<Object> get props => [upcDto];
}

class ZeroOutItemEvent extends UpcEvent implements UpcPageEvent {
  final UpcDto upcDto;

  const ZeroOutItemEvent({this.upcDto});

  @override
  List<Object> get props => [upcDto];
}

class RefreshItemEvent extends UpcEvent implements UpcPageEvent {
  final UpcDto upcDto;

  const RefreshItemEvent({this.upcDto});

  @override
  List<Object> get props => [upcDto];
}

///
/// Marker Interfaces
class UpcPageEvent {}

class ItemPagination {}
