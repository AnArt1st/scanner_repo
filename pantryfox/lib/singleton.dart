import 'dart:collection';

import 'package:device_info/device_info.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'controller/upc_database.dart';
import 'controller/upc_http_controller.dart';
import 'model/user.dart';
import 'package:pantryfox/services/database.dart';

class Singleton {
  static final Singleton _singleton = new Singleton._internal();

  Singleton._internal();

  static Singleton get instance => _singleton;

  User currentUser;

  bool loggedIn = false; //set to true after logging in
  String debuggerName = "Tim"; // Set true to add debugPrinter comments

  /// Store these with user preferences ///
  SharedPreferences _prefs;

  SharedPreferences get prefs {
    if (_prefs == null) {
      SharedPreferences.getInstance().then((value) => {_prefs = value});
    }
    return _prefs;
  }

  set prefs(value) {
    _prefs = value;
  }

  bool databaseInitialized = false;
  UpcSqflite _upcSqflite;

  ///_upcSqflite.init() called in Authenticate.
  ///Don't use this reference until after Authenticate loads as it does the init call
  UpcSqflite get upcSqfliteDb {
    if (databaseInitialized == false) {
      _upcSqflite = new UpcSqflite();
      databaseInitialized = true;
    }
    return _upcSqflite;
  }

  UpcHttpController upcController = new UpcHttpController();

  String firebaseuser = "";

  DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();

  DatabaseService _databaseService;
  DatabaseService get firebaseService {
    if(_databaseService != null) {
      return _databaseService;
    }

    if(currentUser?.uid == null) {
      print("Cannot get firebase reference as user is null");
      return null;
    }

    _databaseService = new DatabaseService(uid: currentUser.uid);
    return _databaseService;
  }
}
