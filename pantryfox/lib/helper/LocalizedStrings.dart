class LocalizedStrings {
  static String helpMessage = "If you don\'t have an account, tap the add user"
      " icon in the bottom left.\n"
      "If you forgot your password, tap \"Forgot Password\".\n"
      "Hit the back arrow in the top left to go back and"
      " select a different way to authenticate.";

  static String userName = "User Name";
  static String pageSize = "Page Size";
  static String orderBy = "Order By";
  static String title = "Title";
  static String description = "Description";
  static String upc = "Upc";
  static String count = "Count";
}