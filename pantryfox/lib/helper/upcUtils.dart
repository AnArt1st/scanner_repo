import 'userUtils.dart';

const nullString = "null";
const noImageUrl = 'images/no_image.jpg';
const noImage = 'images/bag_o_stuff_icon.png';

const VALID_UPC_LENGTH = 12;

///See https://en.wikipedia.org/wiki/Universal_Product_Code
String fixUpcToValidLength(String upcCode) {
  int len = upcCode.length;
  if(len > VALID_UPC_LENGTH) {
    debugPrinter("upcCode is longer than $VALID_UPC_LENGTH = $upcCode");
    debugPrinter("upcCode length = $len");
    int discardX = len - VALID_UPC_LENGTH;
    debugPrinter("discardX length = $discardX");
    debugPrinter("result string = ${upcCode.substring(discardX)}");
    return upcCode.substring(discardX);
  }
  return upcCode;
}