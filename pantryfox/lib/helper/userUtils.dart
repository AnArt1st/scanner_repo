import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:pantryfox/screens/settings/widgets/settings_bloc.dart';
import 'package:pantryfox/bloc/upc_event.dart';
import 'package:pantryfox/bloc/upc_event_state_bloc.dart';
import 'package:pantryfox/screens/create_user/create_user.dart';
import 'package:pantryfox/screens/help/help_screen.dart';
import 'dart:io';
import 'package:device_info/device_info.dart';

import '../singleton.dart';
import 'LocalizedStrings.dart';

const double smallerTextSize = 12.0;
const double smallTextSize = 16.0;
const double mediumTextSize = 20.0;
final smallTextColor = Colors.white;
const int BIGNUM = 2147000000;
const Color someColor = Colors.white;
Color inputFieldColor = Colors.black54;

IconData darknessIcon = Icons.brightness_3;
double iconSize = 30.0;
double buttonHeight = 60.0;
Color iconColor = Colors.white;

List<Color> lightColors = [
  Colors.yellow[200],
  Colors.blueAccent[100],
  Colors.indigo[300],
];
List<Color> darkColors = [
  Colors.blueGrey,
  Colors.indigo,
  Colors.black,
];
List<Color> myColorArray = lightColors;

Widget multiLineTextField(
    String label, TextEditingController control, BuildContext context) {
  return Padding(
    padding: const EdgeInsets.only(left: 8.0, right: 8.0),
    child: Container(
      decoration: new BoxDecoration(
          color: Colors.black38, borderRadius: new BorderRadius.circular(8.0)),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20.0),
        child: new TextField(
          textInputAction: TextInputAction.done,
          keyboardType: TextInputType.multiline,
          maxLines: 3,
          style: new TextStyle(
            color: Colors.white,
          ),
          cursorColor: Colors.white,
          controller: control,
          decoration: new InputDecoration(
            labelStyle: new TextStyle(
              color: Colors.white70,
              fontWeight: FontWeight.bold,
            ),
            labelText: label,
          ),
        ),
      ),
    ),
  );
}

Widget addAppLogo() {
  return AnimatedContainer(
    alignment: Alignment.center,
    width: 250.0,
    height: 250.0,
    decoration: BoxDecoration(
      color: someColor,
      borderRadius: BorderRadius.circular(125.0),
    ),
    duration: Duration(seconds: 1),
    child: ClipRRect(
        borderRadius: new BorderRadius.circular(100.0),
        child: Image.asset(
          'images/fox.png',
          height: 200.0,
          width: 200.0,
        )),
    curve: Curves.fastOutSlowIn,
//          child: Image.asset('images/bag_o_stuff_icon.png'),
  );
}

Widget loadingWheel(BuildContext context) {
  return Padding(
    padding: const EdgeInsets.all(100.0),
    child: SpinKitCircle(
      color: myColorArray[0],
      size:
          MediaQuery.of(context).size.width < MediaQuery.of(context).size.height
              ? MediaQuery.of(context).size.width * 0.25
              : MediaQuery.of(context).size.height * 0.25 ?? 150.0,
      duration: Duration(milliseconds: 1000),
    ),
  );
}

Widget myCustomAlert(BuildContext context, String title, String message) {
  return AlertDialog(
    backgroundColor: myColorArray[1],
    contentTextStyle: TextStyle(
      fontWeight: FontWeight.bold,
    ),
    title: shadowText(title, 18.0),
    content: new Text(message),
    actions: <Widget>[
      // usually buttons at the bottom of the dialog
      new FlatButton(
        child: new Text("Close"),
        onPressed: () {
          Navigator.of(context).pop();
        },
      ),
    ],
  );
}

Widget sortAlert(BuildContext context, String title, String message) {
  return AlertDialog(
    backgroundColor: myColorArray[1],
    contentTextStyle: TextStyle(
      color: Colors.white,
      fontWeight: FontWeight.bold,
    ),
    title: shadowText(title, mediumTextSize),
    content: new Text(message),
    actions: <Widget>[
      // usually buttons at the bottom of the dialog
      Column(
        children: <Widget>[
          _sortItemsButton(context, LocalizedStrings.title),
          _sortItemsButton(context, LocalizedStrings.description),
        ],
      ),
      Column(
        children: <Widget>[
          _sortItemsButton(context, LocalizedStrings.upc),
          _sortItemsButton(context, LocalizedStrings.count),
        ],
      ),
      GestureDetector(
        onTap: () {
          Navigator.of(context).pop();
        },
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            "Cancel",
            style: TextStyle(color: Colors.red),
          ),
        ),
      ),
    ],
  );
}

//Widget mergeAlert(BuildContext context, final GlobalKey<ScaffoldState> _scaffoldKey, dbController.UpcSqflite upcSqflite, var jsonResponse){
//  return AlertDialog(
//    backgroundColor: myColorArray[1],
//    contentTextStyle: TextStyle(
//      color: Colors.white,
//      fontWeight: FontWeight.bold,
//    ),
//    title: shadowText("Are you sure?", mediumTextSize),
//    content: new Text("Merge will remove all items on your device."),
//    actions: <Widget>[
//      GestureDetector(
//        onTap: () async {
//          //Reset items on device.
//          upcSqflite.bean.removeAll();
//          // Get items from backup.
//          for(var upcJson in jsonResponse["testData"]) {
//            if (upcJson == null) break;
//            UpcDb upcDb = UpcDb.fromJson(upcJson);
//
////            if(upcSqflite.bean.findOne(upcDb.code) == null) {
////              // Not found on device.
////            }
//            upcSqflite.bean.addUpcToDevice(upcDb, upcDb.imageLink);
//            print('User is sure.');
//          }
//
//          await upcSqflite.bean.loadDatabaseToSingletonList();
//          upcSqflite.bean.loadUpcEntries();
//          showToast(_scaffoldKey, "Items successfully loaded from database");
//
//          Navigator.of(context).pop();
//        },
//        child: Padding(
//          padding: const EdgeInsets.all(8.0),
//          child: Text("Merge", style: TextStyle(color: Colors.red),),
//        ),
//      ),
//      GestureDetector(
//        onTap: () {
//          Navigator.of(context).pop();
//        },
//        child: Padding(
//          padding: const EdgeInsets.all(8.0),
//          child: Text("Cancel", style: TextStyle(color: Colors.red),),
//        ),
//      ),
//    ],
//  );
//}

// Todo: Make this less messy...
Widget _sortItemsButton(BuildContext context, String sortBy) {
  return FlatButton(
      child: new Text(
        sortBy,
        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
      ),
      onPressed: () {
        debugPrinter("Sorting by " + sortBy);
        Singleton.instance.prefs?.setString(
            SettingsFormBloc?.userName ?? LocalizedStrings.title,
            sortBy.toLowerCase());

        Navigator.of(context).pop();
      });
}

Widget myBottomAppBar(
    BuildContext context, Widget leftButton, Widget rightButton) {
  return BottomAppBar(
    color: myColorArray[1],
    child: new Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[leftButton, rightButton],
    ),
  );
}

Widget addUserButton(BuildContext context) {
  return GestureDetector(
    onTap: () {
      Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => CreateUser(),
          ));
    },
    child: Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        decoration: new BoxDecoration(
            color: myColorArray[2],
            borderRadius: new BorderRadius.circular(18.0)),
        child: Row(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 8.0),
              child: Icon(Icons.person_add, color: iconColor),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                "Add User",
                style:
                    TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
      ),
    ),
  );
}

/// Returns text with a shadow.
Widget shadowText(String text, double fontSize) {
  return Text(
    text,
    style: new TextStyle(
      fontSize: fontSize,
      color: myColorArray[0],
      shadows: <Shadow>[
        Shadow(
          blurRadius: 2.0,
          color: Colors.black87,
        ),
      ],
    ),
  );
}

Widget darkText(String text, double fontSize) {
  return Text(
    text,
    style: new TextStyle(
        fontSize: fontSize, color: Colors.black, fontWeight: FontWeight.bold),
  );
}

Widget myAppBar(String title) {
  return AppBar(
    title: Text(title),
    iconTheme: new IconThemeData(color: myColorArray[1]),
  );
}

Widget myAppBarWithShadowText(
    {String title, Widget rightButton: const Text('')}) {
  return AppBar(
    backgroundColor: myColorArray[1],
    title: shadowText(title, 28.0),
    elevation: 8.0,
    iconTheme: new IconThemeData(color: myColorArray[0]),
    actions: <Widget>[rightButton],
  );
}

/// Add background color
Widget standardContainer(Widget contents) {
  return Container(
    decoration: BoxDecoration(
      // Box decoration takes a gradient
      gradient: LinearGradient(
        // Where the linear gradient begins and ends
        begin: Alignment.topLeft,
        end: Alignment.bottomRight,
        // Add one stop for each color. Stops should increase from 0 to 1
        stops: [0.1, 0.5, 0.9],
        colors: myColorArray,
      ),
    ),
    child: contents,
  );
}

Future<void> showToast(GlobalKey<ScaffoldState> scaffoldKey, String msg) async {
  if (scaffoldKey.currentState == null) {
    debugPrinter('wjndbg: Current state is null in showToast');
    return;
  }
  scaffoldKey.currentState.showSnackBar(SnackBar(
    content: shadowText(msg, smallTextSize),
    backgroundColor: myColorArray[1],
    duration: Duration(seconds: 3),
  ));
}

Widget getTextField(String label, TextEditingController control, bool hidden,
    TextInputType type, BuildContext context) {
  TextField field = TextField(
    textInputAction: TextInputAction.done,
  );
  return Padding(
    padding: const EdgeInsets.only(left: 8.0, right: 8.0),
    child: Container(
      decoration: new BoxDecoration(
          color: Colors.black38, borderRadius: new BorderRadius.circular(8.0)),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20.0),
        child: new TextField(
          textInputAction: field.textInputAction,
          keyboardType: type,
          style: new TextStyle(
            color: Colors.white,
          ),
          cursorColor: Colors.white,
          controller: control,
          obscureText: hidden,
          // Obscure if true.
          decoration: new InputDecoration(
            labelStyle: new TextStyle(
              color: Colors.white70,
              fontWeight: FontWeight.bold,
            ),
            labelText: label,
          ),
        ),
      ),
    ),
  );
}

Widget helpTextContainer(String helpText, String title) {
  return Padding(
    padding: const EdgeInsets.only(left: 8.0, right: 8.0),
    child: Container(
      decoration: new BoxDecoration(
          color: myColorArray[2],
          borderRadius: new BorderRadius.circular(16.0)),
      child: Padding(
        padding: const EdgeInsets.all(32.0),
        child: new Text(
          title + ":\n\n" + helpText,
          style: TextStyle(fontSize: mediumTextSize, color: Colors.white),
        ),
      ),
    ),
  );
}

Widget helpButton(BuildContext context, String helpTitle, String helpMessage) {
  return GestureDetector(
    onTap: () {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return myCustomAlert(context, helpTitle, helpMessage);
        },
      );
    },
    child: Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        decoration: new BoxDecoration(
            color: myColorArray[2],
            borderRadius: new BorderRadius.circular(18.0)),
        child: Row(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 8.0),
              child: Icon(Icons.help, color: Colors.white),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                helpTitle,
                style:
                    TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
      ),
    ),
  );
}

Widget helpButtonListTile(BuildContext context, String helpMessage) {
  return new ListTile(
      title: new Text(helpMessage),
      trailing: new Icon(Icons.help),
      onTap: () {
        Navigator.of(context).pop();
        Navigator.of(context).push(new MaterialPageRoute(
            builder: (BuildContext context) => new HelpScreenPage()));
      });
}

Widget getUsernameText() {
//  DatabaseService service = new DatabaseService(uid: Singleton.instance.currentUser.uid);
  String name =
      Singleton.instance.prefs?.getString(SettingsFormBloc.userName) ??
          "New User";
  return new Text(
    name,
    style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
  );
}

Widget getUserEmailText() {
  String email = Singleton.instance?.currentUser?.email ?? "email";
  return new Text(
    email,

    /// ToDo: Get from current user or file
    style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
  );
}

//Widget pageSizeDropdown(
//    BuildContext context, TextEditingController controller) {
//  return Padding(
//    padding: const EdgeInsets.all(8.0),
//    child: Container(
//      color: inputFieldColor,
//      child: Padding(
//        padding: const EdgeInsets.all(8.0),
//        child: DropdownButton<int>(
////          value: settingsBloc.pageSize,
//          value: Singleton.instance.prefs?.getInt(SettingsFormBloc.pageSize) ?? 6,
//          icon: Icon(Icons.arrow_downward, color: iconColor),
//          iconSize: iconSize,
//          elevation: 16,
//
//          underline: Container(
//            height: 2,
//            color: myColorArray[0],
//          ),
//          onChanged: (int newValue) {
////            settingsBloc.updatePageSize(newValue);
//            controller.text = newValue.toString();
//            Singleton.instance.prefs.setInt(SettingsFormBloc.pageSize, newValue);
//            debugPrinter("wjndbg: pageSize = " + controller.text);
//            reloadStatelessWidget(context, "/scanHomePage");
//          },
//          items:
//              <int>[5, 7, 11, 15, 20].map<DropdownMenuItem<int>>((int value) {
//            return DropdownMenuItem<int>(
//              value: value,
//              child: Text(
//                value != null
//                    ? "Items per page: " + value.toString()
//                    : "Number of items per page",
//                style: TextStyle(
//                  color: myColorArray[1],
//                ),
//              ),
//            );
//          }).toList(),
//        ),
//      ),
//    ),
//  );
//}

void reloadStatelessWidget(BuildContext context, String pageName) async {
  Navigator.pushReplacementNamed(context, pageName);
  debugPrint('Reloading $pageName ...');
}

Widget dbPageNextButton(BuildContext context, UpcEventStateBloc upcBloc) {
  return IconButton(
    icon: Padding(
      padding: const EdgeInsets.all(8.0),
      child: Icon(Icons.keyboard_arrow_right, color: Colors.white),
    ),
    onPressed: () async {
      debugPrinter("wjndbg: dbPageNextButton tapped");
      upcBloc.add(NextPageEvent(offSet: 0, fetchQty: 7));
    },
  );
}

/// move to last
Widget dbLastPageButton(BuildContext context, UpcEventStateBloc upcBloc) {
//  final AuthService _auth = AuthService();
  return IconButton(
    icon: Padding(
      padding: const EdgeInsets.all(8.0),
      child: Icon(Icons.fast_forward, color: Colors.white),
    ),
    onPressed: () async {
      debugPrinter("wjndbg: dbLastPageButton tapped");
      upcBloc.add(LastPageEvent(offSet: 0, fetchQty: 7));
    },
  );
}

/// move to first
Widget dbFirstPageButton(BuildContext context, UpcEventStateBloc upcBloc) {
//  final AuthService _auth = AuthService();
  return IconButton(
    icon: Padding(
      padding: const EdgeInsets.all(8.0),
      child: Icon(Icons.fast_rewind, color: Colors.white),
    ),
    onPressed: () async {
      debugPrinter("wjndbg: dbFirstPageButton tapped");
      upcBloc.add(FirstPageEvent(offSet: 0, fetchQty: 7));
    },
  );
}

Widget dbPagePrevButton(BuildContext context, UpcEventStateBloc upcBloc) {
//  final AuthService _auth = AuthService();
  return IconButton(
    icon: Padding(
      padding: const EdgeInsets.all(8.0),
      child: Icon(Icons.keyboard_arrow_left, color: Colors.white),
    ),
    onPressed: () async {
      debugPrinter("wjndbg: Prev Button tapped");
      upcBloc.add(PrevPageEvent(offSet: 0, fetchQty: 7));
    },
  );
}

Widget sortButton(BuildContext context, String sortMessage) {
  String alertTitle = 'Sort Items';
  return GestureDetector(
    onTap: () {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return sortAlert(context, alertTitle, sortMessage);
        },
      );
    },
    child: Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        decoration: new BoxDecoration(
            color: myColorArray[2],
            borderRadius: new BorderRadius.circular(18.0)),
        child: Row(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 8.0),
              child: Icon(
                Icons.sort,
                color: iconColor,
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                "Sort",
                style:
                    TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
      ),
    ),
  );
}

Widget buttonContainer(
    Color buttonColor, String buttonText, double buttonFontSize) {
  return Container(
      alignment: Alignment.center,
      height: buttonHeight,
      decoration: new BoxDecoration(
          color: buttonColor, borderRadius: new BorderRadius.circular(18.0)),
      child: new Text(buttonText,
          textAlign: TextAlign.center,
          style: new TextStyle(fontSize: buttonFontSize, color: Colors.white)));
}

Widget darknessIconButtonContainer(
    Color buttonColor, String buttonText, double buttonFontSize) {
  return Container(
    alignment: Alignment.center,
    height: buttonHeight,
    decoration: new BoxDecoration(
        color: buttonColor, borderRadius: new BorderRadius.circular(18.0)),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        Icon(
          darknessIcon,
          color: iconColor,
          size: iconSize,
        ),
        new Text(buttonText,
            textAlign: TextAlign.center,
            style: new TextStyle(fontSize: buttonFontSize, color: Colors.white))
      ],
    ),
  );
}

bool isNumeric(String str) {
  if (str == null) {
    return false;
  }
  return num.tryParse(str) != null;
}

/// Can be used to filter debug statements
void debugPrinter(String msg) {
  /// Set to something other that Will and it will be printed.
  switch (Singleton.instance.debuggerName) {
    case "Will":
      {
        if (msg.contains("wjndbg:")) {
          debugPrint(msg);
        }
      }
      break;
    default:
      {
        debugPrint(msg);
      }
      break;
  }
}

class FormButton extends StatelessWidget {
  final VoidCallback onPressed;
  final String text;
  final EdgeInsetsGeometry padding;
  const FormButton({
    Key key,
    @required this.onPressed,
    @required this.text,
    this.padding = const EdgeInsets.all(8.0),
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: padding,
      child: RaisedButton(
        onPressed: onPressed,
        child: Center(child: Text(text)),
      ),
    );
  }
}

class LoadingDialog extends StatelessWidget {
  static void show(BuildContext context, {Key key}) {
    showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (_) => LoadingDialog(key: key),
    );
  }

  static void hide(BuildContext context) {
    Navigator.pop(context);
  }

  LoadingDialog({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Center(child: loadingWheel(context)

//        Card(
//          child: Container(
//            width: 80,
//            height: 80,
//            padding: EdgeInsets.all(12.0),
//            child: CircularProgressIndicator(),
//          ),
//        ),
      ),
    );
  }
}


  Future<String> deviceName() async {
    Map<String, dynamic> deviceData;
    try {
      if (Platform.isAndroid) {
        deviceData = _readAndroidBuildData(await Singleton.instance.deviceInfoPlugin.androidInfo);
      } else if (Platform.isIOS) {
        print("Need to implement device info for ios");
        //deviceData = _readIosDeviceInfo(await deviceInfoPlugin.iosInfo);
      } else {
        print("Platform did not determine the device correctly");
      }
    } on PlatformException {
      deviceData = <String, dynamic>{
        'Error:': 'Failed to get platform version.'
      };
    }
//    String device = deviceData['device'];
//    String id = deviceData['id'];
    String androidId = deviceData['androidId'];
    print("Device Name is $androidId");
    return androidId;
  }

  Map<String, dynamic> _readAndroidBuildData(AndroidDeviceInfo info) {
    return <String, dynamic>{
      'version.securityPatch': info.version.securityPatch,
      'version.sdkInt': info.version.sdkInt,
      'version.release': info.version.release,
      'version.previewSdkInt': info.version.previewSdkInt,
      'version.incremental': info.version.incremental,
      'version.codename': info.version.codename,
      'version.baseOS': info.version.baseOS,
      'board': info.board,
      'bootloader': info.bootloader,
      'brand': info.brand,
      'device': info.device,
      'display': info.display,
      'fingerprint': info.fingerprint,
      'hardware': info.hardware,
      'host': info.host,
      'id': info.id,
      'manufacturer': info.manufacturer,
      'model': info.model,
      'product': info.product,
      'supported32BitAbis': info.supported32BitAbis,
      'supported64BitAbis': info.supported64BitAbis,
      'supportedAbis': info.supportedAbis,
      'tags': info.tags,
      'type': info.type,
      'isPhysicalDevice': info.isPhysicalDevice,
      'androidId': info.androidId,
      'systemFeatures': info.systemFeatures,
    };
  }

