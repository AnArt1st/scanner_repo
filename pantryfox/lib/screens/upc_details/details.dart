import 'dart:async';
import 'dart:core';

import 'package:flutter/material.dart';
import 'package:pantryfox/bloc/upc_event.dart';
import 'package:pantryfox/bloc/upc_event_state_bloc.dart';
import 'package:pantryfox/helper/userUtils.dart';
import 'package:pantryfox/model/upcDto.dart';

class DetailsPage extends StatelessWidget {
  final UpcDto upcDto;
  final UpcEventStateBloc upcBloc;

  final totalController = TextEditingController();
  final titleController = TextEditingController();
  final descriptionController = TextEditingController();
  final brandController = TextEditingController();
  final modelController = TextEditingController();
  final weightController = TextEditingController();

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  DetailsPage(this.upcDto, this.upcBloc) {
    Future.delayed(Duration(milliseconds: 0)).then((_) => executeAfterBuild());
  }

  Future<void> executeAfterBuild() async {
    totalController.text = upcDto.total.toString();
    titleController.text = upcDto.title;
    descriptionController.text = upcDto.description;
    brandController.text = upcDto.brand;
    modelController.text = upcDto.model;
    weightController.text = upcDto.weight;
  }

  @override
  Widget build(BuildContext context) {
    var scaffold = Scaffold(
      key: _scaffoldKey,
      backgroundColor: myColorArray[0],
      appBar: myAppBarWithShadowText(title: 'PantryFox'),
      body: standardContainer(
        GestureDetector(
          child: SingleChildScrollView(
            child: Container(
                child: Column(
              children: <Widget>[
                Container(
                    height: 200.0,
                    padding: const EdgeInsets.only(top: 8.0),
                    child: Image.network(upcDto.imageLink)),
                detailContainer('Upc: ' + upcDto.code),
                detailContainer('Date added: ' +
                    new DateTime.fromMillisecondsSinceEpoch(upcDto.entryTime)
                        .toIso8601String()),
                getTextField('Count', totalController, false,
                    TextInputType.number, context),
                getTextField('Title', titleController, false,
                    TextInputType.text, context),
                multiLineTextField(
                    'Description', descriptionController, context),
                getTextField('Brand', brandController, false,
                    TextInputType.text, context),
                getTextField('Model', modelController, false,
                    TextInputType.text, context),
                getTextField('Weight', weightController, false,
                    TextInputType.text, context),
                submitButton(context),
              ],
            )),
          ),
          onPanDown: (scroll) {
            if (MediaQuery.of(context).viewInsets.bottom != 0.0) {
              // Hide keyboard
              FocusScope.of(context).unfocus();
            }
          },
          onTap: () {
            if (MediaQuery.of(context).viewInsets.bottom != 0.0) {
              // Hide keyboard
              FocusScope.of(context).unfocus();
            }
          },
        ),
      ),
    );
    return scaffold;
  } // End of build Widget

  Widget submitButton(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 5.0),
      child: GestureDetector(
          onTap: () {
            if (MediaQuery.of(context).viewInsets.bottom.floor() != 0) {
              // Hide keyboard
              FocusScope.of(context).unfocus();
            } else {
              print('Already dismissed keyboard.');
            }

            String totalTemp =
                totalController.text == null ? '' : totalController.text;
            num count = num.parse(totalTemp);
            String title =
                titleController.text == null ? '' : titleController.text;

            UpcDto updateUpcDto = new UpcDto(
              code: upcDto.code,
              total: count == null ? 0 : count,
              title: title.isNotEmpty ? title : 'Title',
              description: descriptionController.text,
              imageLink: upcDto.imageLink,
              cupboard: "",
              brand: brandController.text,
              model: modelController.text,
              price: "",
              weight:
                  weightController.text.isNotEmpty ? weightController.text : '',
              selected: false,
              entryTime: 0,
            );
            // Update
            upcBloc.add(RefreshItemEvent(upcDto: updateUpcDto));

            if (MediaQuery.of(context).viewInsets.bottom != 0.0) {
              // Hide keyboard
              FocusScope.of(context).unfocus();
            }
            // Back to upc page
            Navigator.pop(context, true);
          },
          child: buttonContainer(myColorArray[1], 'Submit', mediumTextSize)),
    );
  }

  Widget detailContainer(String detail) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
          decoration: new BoxDecoration(
            color: Colors.white,
            borderRadius: new BorderRadius.all(const Radius.circular(20.0)),
          ),
          child: Center(
              child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: darkText(detail, smallTextSize),
          ))),
    );
  }
}
