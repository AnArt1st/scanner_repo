import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:pantryfox/screens/settings/widgets/settings_bloc.dart';
import 'package:pantryfox/controller/upc_http_controller.dart';
import 'package:pantryfox/helper/LocalizedStrings.dart';
import 'package:pantryfox/helper/Notifications.dart';
import 'package:pantryfox/helper/userUtils.dart';
import 'package:pantryfox/singleton.dart';

class SettingsPage extends StatelessWidget {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final UpcHttpController upcController = Singleton.instance.upcController;

  @override
  Widget build(BuildContext context) {
    debugPrinter('SettingsPage build called');
    return BlocProvider<SettingsFormBloc>(
      create: (context) => SettingsFormBloc(),
      child: Builder(
        builder: (context) {
          return Scaffold(
            appBar: AppBar(title: Text('Settings')),
            body: FormBlocListener<SettingsFormBloc, String, String>(
              onSubmitting: (context, state) {
                LoadingDialog.show(context);
              },
              onSuccess: (context, state) {
                LoadingDialog.hide(context);
                Navigator.pushReplacementNamed(context, '/scanHomePage');
              },
              onFailure: (context, state) {
                LoadingDialog.hide(context);
                debugPrint("failed to save preferences.");
                Notifications.showSnackBarWithError(
                    context, state.failureResponse);
              },
              child: BlocBuilder<SettingsFormBloc, FormBlocState>(
                builder: (context, state) {
                  return ListView(
                    physics: ClampingScrollPhysics(),
                    children: <Widget>[
                      TextFieldBlocBuilder(
                        textFieldBloc:
                            state.textFieldBlocOf(SettingsFormBloc.userName),
                        keyboardType: TextInputType.text,
                        decoration: InputDecoration(
                          labelText: LocalizedStrings.userName,
                          prefixIcon: Icon(Icons.person),
                        ),
                      ),
                      DropdownFieldBlocBuilder(
                        selectFieldBloc:
                            state.selectFieldBlocOf(SettingsFormBloc.orderBy),
                        millisecondsForShowDropdownItemsWhenKeyboardIsOpen: 320,
                        itemBuilder: (context, value) => value,
                        decoration: InputDecoration(
                          labelText: LocalizedStrings.orderBy,
                          prefixIcon: Icon(Icons.sort),
                        ),
                      ),
                      DropdownFieldBlocBuilder(
                        /// Broken widget?
                        selectFieldBloc: state.selectFieldBlocOf(
                            SettingsFormBloc.pageSize ?? "pageSize"),
                        millisecondsForShowDropdownItemsWhenKeyboardIsOpen: 320,
                        itemBuilder: (context, value) => value,
                        decoration: InputDecoration(
                          labelText: LocalizedStrings.pageSize,
                          prefixIcon: Icon(Icons.format_size),
                        ),
                      ),
                      FormButton(
                        text: 'Save',
                        onPressed: () =>
                            context.bloc<SettingsFormBloc>().submit(),
                      ),
                      FormButton(
                        text: 'Backup',
                        onPressed: () => _overwriteBackup(context),
                      ),
                      FormButton(
                        text: 'Restore',
//                        onPressed: () => mergeAlert(context),
                        onPressed: () => _fetchBackup(context),
                      ),
                    ],
                  );
                },
              ),
            ),
          );
        },
      ),
    );
  }

  Future<void> _overwriteBackup(BuildContext context) async {
    print("Overwrite saved backup");
//    await upcController.updatePersnicketyServer()
//        .then((onValue) {
//      showToast(_scaffoldKey, 'Storage has been set.');
//    });
    context.bloc<SettingsFormBloc>().submit();
  }

  Future<void> _fetchBackup(BuildContext context) async {
    print("Fetch saved backup");
    await upcController.fetchPersnicketyData().then((_) {
      context.bloc<SettingsFormBloc>().submit();
      showToast(_scaffoldKey, 'Records loaded!');
    });
  }

  Widget mergeAlert(BuildContext context) {
    return AlertDialog(
      backgroundColor: myColorArray[1],
      contentTextStyle: TextStyle(
        color: Colors.white,
        fontWeight: FontWeight.bold,
      ),
      title: shadowText("Are you sure?", mediumTextSize),
      content: new Text("Merge will remove all items on your device."),
      actions: <Widget>[
        GestureDetector(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              "Merge",
              style: TextStyle(color: Colors.red),
            ),
          ),
          onTap: () async {
            try {
              // Get items from backup.
              _fetchBackup(context);

              Navigator.pushReplacementNamed(context, '/scanHomePage');
            } catch (e) {
              debugPrint("Failed to load backup");
              debugPrint("Exception: $e");
            }
            debugPrint("End of merge");
          },
        ),
        GestureDetector(
          onTap: () {
            Navigator.pushReplacementNamed(context, '/scanHomePage');
          },
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              "Cancel",
              style: TextStyle(color: Colors.red),
            ),
          ),
        ),
      ],
    );
  }

  /// ///////////////////////////////////////////////////

}
