import 'package:easy_localization/easy_localization.dart';
import 'package:form_bloc/form_bloc.dart';
import 'package:pantryfox/helper/LocalizedStrings.dart';
import 'package:pantryfox/helper/userUtils.dart';
import 'package:pantryfox/singleton.dart';

class SettingsFormBloc extends FormBloc<String, String> {
  static final String userName = 'userName';
  static final String pageSize = 'pageSize';
  static final String orderBy = 'orderBy';
  static final String defaultPageSize = "6";

  final userBloc = TextFieldBloc(
      initialValue:
          Singleton.instance?.prefs?.getString(userName) ?? "User not found",
      name: userName,
      validators: [FieldBlocValidators.required]);

  final pageSizeBloc = SelectFieldBloc(
    initialValue: Singleton.instance?.prefs?.getString(pageSize) ?? defaultPageSize,
    name: pageSize,
    items: ["6", "12", "18", "24"],
  );

  final orderByBloc = SelectFieldBloc(
      initialValue: Singleton.instance?.prefs?.getString(orderBy) ??
          LocalizedStrings.title,
      name: orderBy,
      items: [
        LocalizedStrings.title,
        LocalizedStrings.upc,
        LocalizedStrings.description,
        LocalizedStrings.count
      ]);

  void dispose() {
    userBloc.drain();
    pageSizeBloc.drain();
    orderByBloc.drain();
  }

  SettingsFormBloc() {
    addFieldBlocs(fieldBlocs: [userBloc, pageSizeBloc, orderByBloc]);
  }

  @override
  void onSubmitting() async {
    // logic...

    try {
      String saveUserName = state.textFieldBlocOf(userName).value;
      String savePageSize = state.selectFieldBlocOf(pageSize).value;
      String saveOrderBy = state.selectFieldBlocOf(orderBy).value;
      // Get the fields values:
      print("save UserName = " + saveUserName);
      print("save PageSize = " + savePageSize);
      print("save Order by: $saveOrderBy");

      if (Singleton.instance.prefs == null) {
        printWarning("prefs is still null!");
      }

      /// Set preferences
      await Singleton.instance.prefs
          ?.setString(userName, saveUserName ?? LocalizedStrings.userName);
      await Singleton.instance.prefs?.setString(pageSize, savePageSize ?? defaultPageSize);
      await Singleton.instance.prefs
          ?.setString(orderBy, saveOrderBy ?? LocalizedStrings.title);

      String device =  await deviceName();
      await Singleton.instance.firebaseService.updateUserData(saveUserName, Singleton.instance?.currentUser?.email, device, savePageSize);
      /// End set

      /// Success
      emitSuccess();
//      yield state.toSuccess();

    } catch (e) {
      debugPrinter("wjn99d: Error: " + e.toString());
      emitFailure();
//      yield state.toFailure();
    }
  }

//  @override
//  Stream<FormBlocState<String, String>> onLoading() async* {
//    // loading settings...
//    print("loading settings...");
//    print("Page size = " + state.fieldBlocFromPath(pageSize).asTextFieldBloc.value);
//    // In case of infinite loop...
//    await Future<void>.delayed(Duration(seconds: 1));
//    yield state.toLoaded();
//  }

}
