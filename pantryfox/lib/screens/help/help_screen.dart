import 'package:flutter/material.dart';
import 'package:pantryfox/helper/userUtils.dart';


class HelpScreenPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: myAppBarWithShadowText(title: 'Help'),
      body: LayoutBuilder(
        builder: (BuildContext context, BoxConstraints viewportConstraints) {
          return Container(
            height: viewportConstraints.maxHeight,
            color: myColorArray[1],
            child: SingleChildScrollView(
              child: ConstrainedBox(
                constraints: BoxConstraints(
                  minHeight: viewportConstraints.maxHeight,
                ),
                child: IntrinsicHeight(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      helpTextContainer("To sort items in your pantry, "
                          "tap the Sort button in the top right of the items page, "
                          "then tap a category in the dialog to sort by.", "Sort Button"),
                      helpTextContainer("The arrow buttons are for switching pages. "
                          "They are first page, previous page, next page, "
                          "and last page respectively.", "VCR Arrows"),
                    ],
                  ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }

}