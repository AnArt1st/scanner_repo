//import 'package:flutter/material.dart';
//import 'package:flutter_slidable/flutter_slidable.dart';
//import 'package:pantryfox/bloc/upc_event.dart';
//import 'package:pantryfox/bloc/upc_event_state_bloc.dart';
//import 'package:pantryfox/helper/upcUtils.dart';
//import 'package:pantryfox/helper/userUtils.dart';
//import 'package:pantryfox/model/upcDto.dart';
//import 'package:pantryfox/screens/details.dart';
//
//class ScanTile extends StatefulWidget {
//  final UpcDto upcDto;
//  final GlobalKey<ScaffoldState> scaffoldKey;
//  final UpcEventStateBloc upcBloc;
//
//  const ScanTile({
//    Key key,
//    @required this.upcDto,
//    @required this.scaffoldKey,
//    @required  this.upcBloc
//  }) : super(key: key);
//
//  @override
//  _ScanTile createState() => _ScanTile();
//}
//
//class _ScanTile extends State<ScanTile> {
//
//  static const PLENTY = 9000;
//
//
//  @override
//  Widget build(BuildContext context) {
//    return Slidable(
////      controller: ,
//      actionPane: SlidableDrawerActionPane(),
//      actionExtentRatio: 0.25,
//      child: Container(
//        color: myColorArray[0],
//        child: ListTile(
//          leading: Container(
//            color: myColorArray[0],
//              width: 80.0,
//              height: 80.0,
//              child: FadeInImage.assetNetwork(
//                placeholder: noImage,
//                image: widget.upcDto.imageLink,
//              ),
////              Image.network(upcDto.imageLink /*?? noImageUrl*/,
////                loadingBuilder: (context) => loadingWheel(context),
////              )
//          ),
//          title: darkText(widget.upcDto.title ?? "Unknown Item", smallTextSize),
//          subtitle: darkText("Count: ${widget.upcDto.total.toString() ?? '?'}", smallTextSize),
//          contentPadding: const EdgeInsets.all(8.0),
//          onTap: (){
//            Navigator.push(
//                context,
//                //        '/detailsPage', arguments: <UpcDto> {getUpcDtoAtListViewIndex(context, index)}
//                MaterialPageRoute(builder: (context) => DetailsPage(widget.upcDto))
//            ).then((val) => {
//              debugPrinter("wjndbg: Upc = ${widget.upcDto.code} <-> upc code")
//            });
//          },
//        ),
//      ),
//      actions: <Widget>[
//        IconSlideAction(
//          color: Colors.redAccent,
//          icon: Icons.delete_outline,
//          onTap: () {
//            if (widget.upcDto != null) {
//              setState(() => widget.upcBloc.add(ZeroOutItemEvent(upcDto: widget.upcDto)));
//              showToast(widget.scaffoldKey, 'Subtracted one.');
//            } else {
//              debugPrinter("Null upcDto?? ScanTile line 44");
//            }
//          },
//        ),
//      ],
//      secondaryActions: <Widget>[
//        IconSlideAction(
//          color: Colors.blue,
//          icon: Icons.add_box,
//          onTap: () {
//            if(widget.upcDto.total <= PLENTY) {
////                        setState(() => upcBloc.add(IncrementItemEvent(index: index)));
//              setState(() => widget.upcBloc.add(IncrementItemEvent(upcDto: widget.upcDto)));
//              showToast(widget.scaffoldKey, 'Added one.');
//            } else {
//              debugPrint("You have ${widget.upcDto.total}. It's over 9000!!");
//              showToast(widget.scaffoldKey, 'You have plenty of this item.');
//            }
//          },
//        ),
//        IconSlideAction(
//          color: Colors.indigo,
//          icon: Icons.indeterminate_check_box,
//          onTap: () {
//            if (widget.upcDto.total > 0) {
////                        setState(() => upcBloc.add(DecrementItemEvent(index: index)));
//              setState(() => widget.upcBloc.add(DecrementItemEvent(upcDto: widget.upcDto)));
//              showToast(widget.scaffoldKey, 'Subtracted one.');
//            } else {
//              showToast(widget.scaffoldKey, 'You are out of this item.');
//            }
//          },
//        ),
//      ],
//    );
//  }
//
//
//}


