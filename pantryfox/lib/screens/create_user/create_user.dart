import 'dart:async';

import 'package:crypto/crypto.dart';
import 'package:flutter/material.dart';
import 'package:pantryfox/controller/users_database.dart';
import 'package:pantryfox/model/user.dart';
import 'package:pantryfox/helper/userUtils.dart';
import 'package:pantryfox/services/firebaseAuth.dart';


class CreateUser extends StatefulWidget {
  final Function toggleView;
  CreateUser({ this.toggleView });
  @override
  _CreateUserState createState() => _CreateUserState();
}

class _CreateUserState extends State<CreateUser> {
  final AuthService _auth = AuthService();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  String result = "";
  Hash hasher = sha1;
  final helpMessage = 'Email must be a valid address.\n'
      'Password must be at least 8 characters.\n'
      'Must contain one lowercase and one uppercase letter.\n'
      'Must contain a number.\n'
      'Password is case sensitive.\n'
      'Password must be entered the same way in both password fields.';
  final invalidPassword = 'Password must be at least 8 characters.\n';

  final nameController = TextEditingController();
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  final checkPasswordController = TextEditingController();

  @override
  void dispose() {
    // Clean up the controller when the Widget is disposed
    nameController.dispose();
    emailController.dispose();
    passwordController.dispose();
    checkPasswordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: myAppBarWithShadowText(title: 'Create An Account'),
        bottomNavigationBar: myBottomAppBar(
            context,
            shadowText(' ', smallTextSize),
            helpButton(context, "Help", helpMessage)
        ),
        body: standardContainer(
            Center(
              child: SingleChildScrollView(
                  child: Column(
                    children: <Widget>[
                      getTextField('Name', nameController, false, TextInputType.text, context),
                      new SizedBox(
                        height: 10.0,
                      ),
                      getTextField('Email', emailController, false, TextInputType.emailAddress, context),
                      new SizedBox(
                        height: 10.0,
                      ),
                      getTextField('Password', passwordController, true, TextInputType.text, context),
                      new SizedBox(
                        height: 10.0,
                      ),
                      getTextField('Re-enter Password', checkPasswordController, true, TextInputType.text, context),
                      new SizedBox(
                        height: 10.0,
                      ),
                      submitButton(context),
                    ],
                  )
              ),
            )
        ) // End standardContainer.

        );
  }

  Widget submitButton(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 5.0),
      child: GestureDetector(
        onTap: () async {
          if (nameController.text.length < 1) {
            print("Enter a name...");
            alertInvalidField('No Name Entered',
                'Enter a name and try again.');
          } else if (emailController.text.length < 4 || !emailController.text.contains('@')) {
            print("Enter an email...");
            alertInvalidField('No Email Entered',
                'Enter a valid email address and try again.');
          } else if (passwordController.text != checkPasswordController.text) {
            print('Password does not match.');
            alertInvalidField('Password Fields not Matching',
                'Re-enter password field(s) and try again.');
          } else if (passwordController.text.length < 8 ||
              !passwordController.text.contains(new RegExp(r'[a-z]')) ||
              !passwordController.text.contains(new RegExp(r'[A-Z]')) ||
              !passwordController.text.contains(new RegExp(r'[0-9]'))) {
            print(helpMessage);
            alertInvalidField('Invalid Password Format', helpMessage);
          } else {
            /// TODO: Add a variable to say loading next screen.
            /// TODO: Check if email already exists.
            User user = new User();
            String password = passwordController.text;

            user.email = emailController.text.toLowerCase().trim();

            //Register in Firebase
            dynamic fbUser = await _auth.registerWithEmailAndPassword(user.email, password, _scaffoldKey);
            insertUserToDatabase(fbUser);

            Navigator.pop(context); // Goes back to login page.
          }
        },
        child: buttonContainer(myColorArray[2], 'Submit', mediumTextSize)
      ),
    );
  }

  // ignore: missing_return
  Future<void> insertUserToDatabase(User user) async {
    UsersSqflite usersSqflite;

    usersSqflite = new UsersSqflite();
    await usersSqflite.init();
    await usersSqflite.bean.insert(user);
  }

  // Alert user of an invalid field.
  void alertInvalidField(String messageTitle, String message) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return myCustomAlert(context, messageTitle, message);
      },
    );
  }
} //End of Class
