import 'package:flutter/material.dart';
import 'package:pantryfox/singleton.dart';
import 'package:pantryfox/screens/homepage/homePage.dart';
import 'package:provider/provider.dart';
import 'package:pantryfox/model/user.dart';
import 'package:pantryfox/screens/authenticate.dart';


class AuthWrapper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final user = Singleton?.instance?.currentUser;  //Provider.of<User>(context);
    // return either the Home or Authenticate widget
    if (user == null){
      print("wrapper.dart User is: null - calling Authenticate");
      return Authenticate();
    } else {
      print("wrapper.dart User is: "+user.toString());
      return ScanHomePage();
    }
  }
}
