import 'package:flutter/material.dart';
import 'package:pantryfox/controller/upc_database.dart';
import 'package:pantryfox/screens/create_user/create_user.dart';
import 'package:pantryfox/screens/login/login.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../singleton.dart';

class Authenticate extends StatefulWidget {
  @override
  _AuthenticateState createState() => _AuthenticateState();
}

class _AuthenticateState extends State<Authenticate> {
  bool showSignIn = true;

  @override
  initState() {
    _getThingsOnStartup().then((val) {
      print("_getThingsOnStartup completed");
    });
    super.initState();
  }

  Future _getThingsOnStartup() async {
    UpcSqflite upcSqflite = Singleton.instance.upcSqfliteDb;
    if (Singleton.instance.prefs == null) {
      SharedPreferences.getInstance()
          .then((value) => {Singleton.instance.prefs = value});
    }
    await Singleton.instance.upcSqfliteDb.init(null).then((val) {
      if (upcSqflite.bean != null) {
        print("upcSqfliteDb.init completed");
      } else {
        print("upcSqflite bean is null");
      }
    });
    //await Future.delayed(Duration(seconds: 2));
  }

  void toggleView() {
    print("showSignIn before toggle flip ${showSignIn.toString()}");
    setState(() => showSignIn = !showSignIn);
  }

  @override
  Widget build(BuildContext context) {
    if (showSignIn) {
      print("authenticate calling LoginPage ${showSignIn.toString()}");
      return LoginPage();
    } else {
      print("authenticate calling CreateUser ${showSignIn.toString()}");
      return CreateUser(toggleView: toggleView);
    }
  }
}
