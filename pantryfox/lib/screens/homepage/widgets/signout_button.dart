import 'package:flutter/material.dart';
import 'package:pantryfox/helper/userUtils.dart';
import 'package:pantryfox/services/firebaseAuth.dart';

Widget signoutButton(BuildContext context) {
  final AuthService _auth = AuthService();

  return GestureDetector(
    onTap: () async {
      await _auth.signOut();
      debugPrinter("Signing out - pushing to AuthWrapper");
      Navigator.pushReplacementNamed(context, '/loginPage');
    },
    child: buttonContainer(myColorArray[1], "Sign Out", smallerTextSize),
  );
}
