import 'package:flutter/material.dart';
import 'package:pantryfox/screens/homepage/widgets/signout_button.dart';
import 'package:pantryfox/screens/settings/widgets/settings_bloc.dart';
import 'package:pantryfox/helper/userUtils.dart';
import 'package:pantryfox/singleton.dart';

Widget getDrawer(BuildContext context) {
  return new Drawer(
    child: new ListView(
      children: <Widget>[
        new UserAccountsDrawerHeader(
          accountName: getUsernameText(),
          currentAccountPicture: new GestureDetector(
            child: new CircleAvatar(
              backgroundImage: new AssetImage(
                  'images/blank-profile-picture-973460_1280.png'),
            ),
            onTap: () => print("This is your current account."),
          ),
          otherAccountsPictures: <Widget>[
            signoutButton(context),
          ],
          decoration: new BoxDecoration(
              color: myColorArray[0],
              shape: BoxShape.circle,
              image: new DecorationImage(
                  image: AssetImage('images/fox.png'), fit: BoxFit.fitHeight)),
          accountEmail: getUserEmailText(),
        ),
        new ListTile(
            title: new Text("Settings"),
            trailing: new Icon(Icons.settings),
            onTap: () {
              debugPrinter("wjndbg: Page size is " +
                  (Singleton.instance.prefs
                          ?.getString(SettingsFormBloc.pageSize) ??
                      "zero") +
                  " items.");
              Navigator.pushReplacementNamed(context, '/settings');
            }),
        helpButtonListTile(context, "Help"),
        new Divider(),
        new ListTile(
          title: new Text("Cancel"),
          trailing: new Icon(Icons.cancel),
          onTap: () => Navigator.pop(context),
        ),
      ],
    ),
  );
}
