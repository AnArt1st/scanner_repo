import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:pantryfox/bloc/upc_event.dart';
import 'package:pantryfox/bloc/upc_event_state_bloc.dart';
import 'package:pantryfox/helper/upcUtils.dart';
import 'package:pantryfox/helper/userUtils.dart';
import 'package:pantryfox/model/upcDto.dart';
import 'package:pantryfox/screens/upc_details/details.dart';

Widget scanTile(BuildContext context, UpcDto upcDto, UpcEventStateBloc upcBloc,
    GlobalKey<ScaffoldState> _scaffoldKey) {
  const PLENTY = 9000;

  return Slidable(
    actionPane: SlidableDrawerActionPane(),
    actionExtentRatio: 0.25,
    child: Container(
      height: MediaQuery.of(context).size.height * 0.15,
      decoration: BoxDecoration(
          color: Colors.white60,
          border: Border(
              top: BorderSide(color: myColorArray[2]),
              bottom: BorderSide(color: myColorArray[2]))),
      child: Center(
        child: ListTile(
          leading: Container(
            width: MediaQuery.of(context).size.width * 0.15,
            height: MediaQuery.of(context).size.width * 0.15,
            decoration: new BoxDecoration(
              borderRadius: new BorderRadius.circular(
                  MediaQuery.of(context).size.width * 0.1),
              color: Colors.white,
            ),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(
                  MediaQuery.of(context).size.width * 0.1),
              child: FadeInImage.assetNetwork(
                placeholder: noImage,
                image: upcDto.imageLink,
              ),
            ),
          ),
          title: darkText(upcDto.title ?? "Unknown Item", smallTextSize),
          subtitle: darkText(
              "Count: ${upcDto.total.toString() ?? '?'}", smallTextSize),
          contentPadding: const EdgeInsets.all(8.0),
          onTap: () {
            Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => DetailsPage(upcDto, upcBloc)))
                .then((val) => {
                      // upcBloc.add(RefreshItemEvent(upcDto: upcDto)),
                      debugPrinter(
                          "wjndbg: image = ${upcDto.imageLink} <-> upc image")
                    });
          },
        ),
      ),
    ),
    actions: <Widget>[
      IconSlideAction(
        color: Colors.redAccent,
        icon: Icons.delete_outline,
        onTap: () {
          if (upcDto != null) {
            upcBloc.add(ZeroOutItemEvent(upcDto: upcDto));
            showToast(_scaffoldKey, 'You are out of this item');
          } else {
            debugPrinter("Null upcDto?? ScanTile line 44");
          }
        },
      ),
    ],
    secondaryActions: <Widget>[
      IconSlideAction(
        color: Colors.blue,
        icon: Icons.add_box,
        onTap: () {
          if (upcDto.total <= PLENTY) {
            upcBloc.add(IncrementItemEvent(upcDto: upcDto));
            showToast(_scaffoldKey, 'Added one.');
          } else {
            debugPrint("You have ${upcDto.total}. It's over 9000!!");
            showToast(_scaffoldKey, 'You have plenty of this item');
          }
        },
      ),
      IconSlideAction(
        color: Colors.indigo,
        icon: Icons.indeterminate_check_box,
        onTap: () {
          if (upcDto.total > 0) {
            upcBloc.add(DecrementItemEvent(upcDto: upcDto));
            showToast(_scaffoldKey, 'Subtracted one');
          } else {
            showToast(_scaffoldKey, 'You are out of this item');
          }
        },
      ),
    ],
  );
}
