import 'dart:async';

import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pantryfox/bloc/upc_event.dart';
import 'package:pantryfox/bloc/upc_event_state_bloc.dart';
import 'package:pantryfox/controller/upc_http_controller.dart';
import 'package:pantryfox/helper/upcUtils.dart';
import 'package:pantryfox/helper/userUtils.dart';
import 'package:pantryfox/model/upcDb.dart';
import 'package:pantryfox/model/upcDto.dart';
import 'package:pantryfox/model/upcJson.dart';

import '../../../singleton.dart';

/// QR Scanner function //////////////////////////////////////////////////////

Future<void> scanQR(BuildContext context, UpcEventStateBloc upcBloc,
    GlobalKey<ScaffoldState> _scaffoldKey) async {
  bool hideDialog = true;
  try {
    ScanResult qrResult = await BarcodeScanner.scan();
    if (qrResult == null) {
      showToast(_scaffoldKey, 'Item scan unsuccessful.');
      return;
    }
    debugPrinter("_scanQR start");
    if (qrResult?.rawContent != null &&
        qrResult.rawContent.length >= VALID_UPC_LENGTH) {
      debugPrinter("_scanQR showDialog");
      showDialog(
        context: context,
        builder: (BuildContext context) => loadingWheel(context),
      ).then((value) {
        hideDialog = false;
      });
    }
    UpcDto upcDbResult = await upcBloc.findUpcDbInDatabase(qrResult.rawContent);
    if (upcDbResult != null && upcDbResult.code != nullString) {
      debugPrinter("calling updateTotalByOne");
      await upcBloc.updateTotal(upcDbResult, 1);
      // Scroll and update count.
      hideDialog = hideLoadingWheel(context, hideDialog);
      debugPrinter("Updated item on good scan.");

      if (_scaffoldKey.currentState == null) {
        debugPrinter("wjndbg: State is null?");
      }
      showToast(_scaffoldKey, 'Updated item on good scan.');
    } else {
      /// actual scan
      debugPrinter("calling _tryScan");
      bool goodToast = await _tryScan(context, qrResult.rawContent, upcBloc);
      hideDialog = hideLoadingWheel(context, hideDialog);
      if (goodToast == true) {
        debugPrinter("Item scan successful.");
        upcBloc.add(RefreshItemEvent(upcDto: upcDbResult));
        showToast(_scaffoldKey, 'Item scan successful.');
      } else {
        debugPrinter("Item scan unsuccessful.");
        showToast(_scaffoldKey, 'Item scan unsuccessful.');
      }
    }
    //hideDialog = hideLoadingWheel(context, true);
    return;
  } on PlatformException catch (ex) {
    debugPrinter("BarcodeScanner Some platform exception: $ex");
  } on FormatException {
    debugPrinter("You pressed the back button before scanning anything");
  } on TimeoutException catch (_e) {
    debugPrinter('Timeout occured: ' + _e.toString());
    hideDialog = hideLoadingWheel(context, hideDialog);
    showToast(_scaffoldKey, 'Item scan unsuccessful');
  }
  debugPrinter("Error exception when item was scanned");
  hideDialog = hideLoadingWheel(context, hideDialog);
  showToast(_scaffoldKey, 'ERROR');
}

/// //////////////////////////////////////////////////////////// end _scan()
///
Future<bool> _tryScan(
    BuildContext context, String qrResult, UpcEventStateBloc upcBloc) async {
  UpcHttpController upcController = Singleton.instance.upcController;

  Upc upcResult;
  upcResult = await Future.value(upcController.getFutureUpcData(qrResult))
      .timeout(const Duration(seconds: 10));
  if (upcResult != null) {
    bool itemsIsEmptyAdded = itemsIsEmpty(upcResult, qrResult);
//        upcController.debugDumpScan(upcResult);

    if (upcResult.items[0].title == null) {
      upcResult.items[0].title = 'No Title Found';
    }
    if (upcResult.items[0].images.isEmpty) {
      debugPrinter('Check that images array is set on items.');
      upcResult.items[0].images.add(noImageUrl);
    }

    var imageUrl = await upcController.getWorkingImageUrl(upcResult);
    if (imageUrl == null) {
      debugPrinter("upcController.getWorkingImageUrl(upcResult) is null");
      imageUrl = noImageUrl;
    }

    if (upcResult.items.isEmpty) {
      return false;
    }

    /// add upc to device storage.
    await addUpcToDevice(upcResult, imageUrl, upcBloc);
  } else {
    return false;
  }

  //Set the list of UpcDb items to be displayed in ListView.
  int numberOfStoredItems = await upcBloc.countItemsScanned();
  debugPrinter(
      'You have ' + numberOfStoredItems.toString() + ' scanned into storage.');
  return true;
}

///
///
bool itemsIsEmpty(Upc upcResult, String qrResult) {
  if (upcResult == null || qrResult == null) {
    debugPrinter("wjndbg: Null stuff in itemsIsEmpty");
    return null;
  }

  if (upcResult.items.isEmpty) {
    upcResult.items = new List<Item>();
    upcResult.items.add(emptyItem(qrResult));
    if (upcResult.total == null) {
      upcResult.total = 1;
    }
    if (upcResult.code == null || upcResult.code.isEmpty) {
      upcResult.code = qrResult; //Assuming a valid scan.
    }

    debugPrinter('Added empty item. Check for nulls.');
    return true;
  }
  return false;
}

///
///
bool hideLoadingWheel(BuildContext context, bool hide) {
  // Hide loading thingy.
  if (hide) {
    Navigator.of(context, rootNavigator: hide).pop();
  }
  return false; // Is now hidden.
}

///
///
Item emptyItem(String upc) {
  String title = 'Empty';
  String descr = 'No description given.';
  List<String> imageList = new List<String>();
  imageList.add(noImageUrl);

  return new Item(
    upc: upc,
    title: title,
    description: descr,
    images: imageList,
  );
}

///
///
// Add Upc
Future<void> addUpcToDevice(
    Upc upc, String goodImageUrl, UpcEventStateBloc upcBloc) async {
  if (upc.items.isEmpty || upc.items == null) {
    debugPrinter("addUpcToDevice - upc.items.isEmpty || upc.items == null");
    return null;
  }
  //Upc from remote needs to be converted to UpcDb -> UpcDto
  //TODO: see if we can eliminate the UpcDto class. It was meant to be a UI version, and Bloc uses it.
  UpcDb upcDb = Singleton.instance.upcController.getUpcDb(upc);
  if (upcDb.code == null || upcDb.code.isEmpty) {
    debugPrinter("Error - Invalid upc code");
    return;
  }
  UpcDto upcDtoFromRemote = UpcDto.getUpcDto(upcDb);
  UpcDto foundUpcDto = await upcBloc.findUpcDbInDatabase(upcDtoFromRemote.code);
  if (foundUpcDto == null ||
      foundUpcDto.code.isEmpty ||
      foundUpcDto.code == null ||
      foundUpcDto.code == nullString) {
    debugPrinter('Not found in database: calling addUpcToDevice.');
    await upcBloc.addUpcToDevice(upcDtoFromRemote, goodImageUrl);
  } else {
    await upcBloc.updateTotal(foundUpcDto, 1);
  }

  debugPrinter('Returning: Updating device.');
  //add upc
}
