import 'dart:async';
import 'dart:core';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pantryfox/bloc/upc_event.dart';
import 'package:pantryfox/bloc/upc_event_state_bloc.dart';
import 'package:pantryfox/screens/homepage/widgets/scan_page_bottom_nav_bar.dart';
import 'package:pantryfox/screens/settings/widgets/settings_bloc.dart';
import 'package:flutter/material.dart';
import 'package:pantryfox/helper/userUtils.dart';

import '../../singleton.dart';
import 'widgets/drawer_menu.dart';
import 'widgets/scan_tile.dart';

class ScanHomePage extends StatelessWidget {
  final _scrollController = ScrollController();

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  ScanHomePage() {
    _scrollController.addListener(_onScroll);
  }

  /// Build Build Build Build Build Build Build Build Build Build ///
  @override
  Widget build(BuildContext context) {
    final UpcEventStateBloc upcBloc =
        BlocProvider.of<UpcEventStateBloc>(context);
    upcBloc.add(FirstPageEvent());
    debugPrinter('Scanpage build called');
    return Scaffold(
      key: _scaffoldKey,
      appBar: myAppBarWithShadowText(
          title: "PantryFox",
          rightButton: sortButton(
              context,
              'Tap the button to sort items by that attribute.'
              ' Or hit cancel to go back.')),
      drawer: getDrawer(context),
      bottomNavigationBar:
          scanPageBottomNavigationBar(context, upcBloc, _scaffoldKey),
      body: BlocBuilder<UpcEventStateBloc, UpcState>(
        buildWhen: (previous, current) {
          print("Previous state = $previous, Current state = $current");
          // Build on transition. Hopefully.
          return previous != current;
        },
        bloc: upcBloc,
        builder: (context, state) {
          if (state is UpcLoadedDtoState) {
            debugPrinter(
                'wjndbg: BlocBuilder returning ListView with ScanTile');
            return standardContainer(
              ListView.builder(
                itemBuilder: (BuildContext context, int index) {
                  return scanTile(
                      context, state.upcDtoList[index], upcBloc, _scaffoldKey);
                },
                itemCount: upcBloc.state.props.length,
                controller: _scrollController,
              ),
            );
          } else {
            return _buildEmptyResults(context, upcBloc);
          }
        },
      ),
    );
  }

  void _onScroll() {
    final maxScroll = _scrollController.position.maxScrollExtent;
    final currentScroll = _scrollController.position.pixels;
    if (currentScroll >= maxScroll) {
      /// Dispatch?
      debugPrint("Bottom of page hit.");
    }
  }

  Widget _buildEmptyResults(BuildContext context, UpcEventStateBloc upcBloc) {
    upcBloc = BlocProvider.of<UpcEventStateBloc>(context);
    int quantity = int.parse(
        Singleton.instance?.prefs?.getString(SettingsFormBloc.pageSize) ?? SettingsFormBloc.defaultPageSize);
    upcBloc.add(FirstPageEvent(offSet: 0, fetchQty: quantity));
    return standardContainer(
        Center(child: Text("Tap 'Scan' to start storing data.")));
  }

  Future<void> executeAfterBuild() async {
    String welcomeMsg = 'Scanner is ready';

    // this code will get executed after the build method
    showToast(_scaffoldKey, welcomeMsg);
  }
} //end class
