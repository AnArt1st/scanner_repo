import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:pantryfox/screens/login/widgets/login_bloc.dart';
import 'package:pantryfox/screens/login/widgets/login_form.dart';
import 'package:pantryfox/helper/LocalizedStrings.dart';
import 'package:pantryfox/helper/userUtils.dart';

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => LoginBloc(),
      child: Builder(
        builder: (context) {
          final formBloc = BlocProvider.of<LoginBloc>(context);

          return Theme(
            data: Theme.of(context).copyWith(
              inputDecorationTheme: InputDecorationTheme(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
              ),
            ),
            child: Scaffold(
              appBar: myAppBarWithShadowText(title: 'Sign in to Pantry Fox'),
              bottomNavigationBar: myBottomAppBar(
                  context,
                  addUserButton(context),
                  helpButton(context, "Help", LocalizedStrings.helpMessage)),
              body: FormBlocListener<LoginBloc, String, String>(
                onSubmitting: (context, state) {
                  LoadingDialog.show(context);
                },
                onSuccess: (context, state) {
                  LoadingDialog.hide(context);

                  Navigator.pushReplacementNamed(context, '/scanHomePage');
                },
                onFailure: (context, state) {
                  LoadingDialog.hide(context);

                  Scaffold.of(context).showSnackBar(
                      SnackBar(content: Text(state?.failureResponse ?? "Unknown failure")));
                },
                child: LoginForm(form: formBloc, context: context),
              ),
            ),
          );
        },
      ),
    );
  }
}
