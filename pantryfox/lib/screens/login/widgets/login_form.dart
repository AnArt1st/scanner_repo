import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:pantryfox/helper/userUtils.dart';

class LoginForm extends StatelessWidget {
  final form;
  final context;

  LoginForm({@required this.form, @required this.context}) : super();

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
        physics: ClampingScrollPhysics(),
        child: Padding(
          padding: const EdgeInsets.all(44.0),
          child: Column(
            children: <Widget>[
              TextFieldBlocBuilder(
                textFieldBloc: form.emailField,
                decoration: InputDecoration(
                  labelText: 'Email',
                  prefixIcon: Icon(Icons.email),
                ),
              ),
              TextFieldBlocBuilder(
                textFieldBloc: form.passwordField,
                suffixButton: SuffixButton.obscureText,
                // obscureText: true,
                obscureTextFalseIcon: Icon(Icons.remove_red_eye),
                obscureTextTrueIcon: Icon(Icons.visibility_off),
                decoration: InputDecoration(
                  labelText: 'Password',
                  prefixIcon: Icon(Icons.vpn_key),
                ),
              ),
              CheckboxFieldBlocBuilder(
                booleanFieldBloc: form.rememberMeField,
                body: Container(
                  alignment: Alignment.centerLeft,
                  child: Text('Remember Me'),
                ),
              ),
              new Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  loginButton(context),
                  forgotPasswordButton(context)
                ],
              ),
            ],
          ),
        ));
  }

  Widget loginButton(BuildContext context) {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.only(
            left: 20.0, right: 5.0, top: 20.0, bottom: 0.0),
        child: GestureDetector(
          onTap: form.submit,
          child: buttonContainer(myColorArray[2], "Login", mediumTextSize),
        ),
      ),
    );
  }

  Widget forgotPasswordButton(BuildContext context) {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.only(left: 10.0, right: 20.0, top: 20.0),
        child: GestureDetector(
          onTap: () {
            print('Are you sure you want to send password reset email?');
            // ignore: unnecessary_statements
            form?.addErrors;
          },
          child: buttonContainer(
              myColorArray[2], 'Forgot Password', smallTextSize),
        ),
      ), // GestureDetector
    );
  }
}
