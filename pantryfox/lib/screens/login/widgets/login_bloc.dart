import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:pantryfox/helper/persistence_helper.dart';
import 'package:pantryfox/model/user.dart';
import 'package:pantryfox/services/firebaseAuth.dart';

import '../../../singleton.dart';

class LoginBloc extends FormBloc<String, String> {
  final AuthService _auth = AuthService();
  final PersistenceHelper _userHelper = new PersistenceHelper();

  static final String emailName = 'email';
  static final String passwordName = 'password';
  static final String rememberMeName = 'rememberMe';

  final emailField = TextFieldBloc(
      name: emailName,
      validators: [FieldBlocValidators.required, FieldBlocValidators.email]);

  final passwordField = TextFieldBloc(name: passwordName, validators: [
    FieldBlocValidators.required,
    FieldBlocValidators.passwordMin6Chars
  ]);

  final rememberMeField =
      BooleanFieldBloc(initialValue: true, name: rememberMeName);

  void addErrors() {
    emailField.addFieldError('Email is required');
    passwordField.addFieldError('Password is required');
  }

  void dispose() {
    emailField.drain();
    passwordField.drain();
    rememberMeField.drain();
  }

  LoginBloc() {
    _userHelper.loadUser().then((val) {
      emailField.updateInitialValue(val.email);
      passwordField.updateInitialValue(val.password);
    });
    addFieldBlocs(fieldBlocs: [emailField, passwordField, rememberMeField]);
  }

  @override
  void onError(Object error, StackTrace stackTrace) {
    addErrors();
    super.onError(error, stackTrace);
  }

  @override
  void onSubmitting() async {
    try {
      /// Check email and password.
      String userEmail = emailField.value;
      String userPass = passwordField.value;
      if (userEmail.length > 1 && userPass.length > 5) {
        /// Find user. Then...
        User user = await _auth.signInWithEmailAndPassword(userEmail, userPass);

        if (user == null) throw AuthException("404", "User not found.");

        if (rememberMeField.value == true) {
          await _userHelper.storeUser(userEmail, userPass);
          Singleton.instance?.prefs?.setString(emailName, userEmail);
        } else {
          // Forget user.
          await _userHelper.removeUser(); // Clears user fields
          Singleton.instance?.prefs?.setString(emailName, null);
        }
        if (user != null && Singleton.instance.currentUser != null) {
          Singleton.instance.currentUser.email = userEmail;
        }

        emitSuccess();
      } else {
        emitFailure();
        print("No user found when mapping login to state");
      }
    } catch (e) {
      emitFailure();
    }
  }
}
